## [2.2.3](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.2.2...v2.2.3) (2022-01-19)


### Bug Fixes

* build lib ES2015 ([8401ed7](http://bitbucket.org/thermsio/rpc-client-ts/commits/8401ed7663b5cf7a2a3a3e9c9a0bc5019765e7d2))

## [2.2.2](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.2.1...v2.2.2) (2022-01-06)


### Bug Fixes

* update deps and tsconfig sourcemaps ([7b30bf9](http://bitbucket.org/thermsio/rpc-client-ts/commits/7b30bf93769d73223c80f30c9520ed50397ec705))

## [2.2.1](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.2.0...v2.2.1) (2021-12-06)


### Bug Fixes

* add debug logs to WebSocketTransport ([9954900](http://bitbucket.org/thermsio/rpc-client-ts/commits/9954900bd40fd63cf9bcd1108de504af640515be))

# [2.2.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.1.2...v2.2.0) (2021-10-02)


### Features

* added registerResponseInterceptor registerRequestInterceptor for adding additional interceptors after initialization ([72ac5be](http://bitbucket.org/thermsio/rpc-client-ts/commits/72ac5bec8a726be5503c8e089d855c88aa91244a))

## [2.1.2](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.1.1...v2.1.2) (2021-10-01)


### Bug Fixes

* persist identity on calls and override identity on single calls ([05e8394](http://bitbucket.org/thermsio/rpc-client-ts/commits/05e839448d4988235a85bb75d1962ab8e4ac74cc))

## [2.1.1](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.1.0...v2.1.1) (2021-09-22)


### Bug Fixes

* return  not optional ([7b2553a](http://bitbucket.org/thermsio/rpc-client-ts/commits/7b2553a8e9b650834a72190db9f2097a4dd98faf))

# [2.1.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v2.0.0...v2.1.0) (2021-09-19)


### Features

* add Args/Data types for call() and getCallCache() methods ([c602066](http://bitbucket.org/thermsio/rpc-client-ts/commits/c602066d8517e2882ab737d1bdfe2851ef621290))

# [2.0.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.2.4...v2.0.0) (2021-09-18)


### Code Refactoring

* export types ([fd30709](http://bitbucket.org/thermsio/rpc-client-ts/commits/fd30709aa040beb050e30298352be979e98e18a1))


### BREAKING CHANGES

* no longer a UMD build in npm package

## [1.2.4](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.2.3...v1.2.4) (2021-05-19)


### Bug Fixes

* update deps ([b6c5bea](http://bitbucket.org/thermsio/rpc-client-ts/commits/b6c5beac403e11e7567838f58ecf86ebbd992722))

## [1.2.3](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.2.2...v1.2.3) (2021-05-18)


### Bug Fixes

* shorthand procedure string scope::procedure::version ([95670d0](http://bitbucket.org/thermsio/rpc-client-ts/commits/95670d047ca1d9306ca0dc7156215df051122e2d))

## [1.2.2](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.2.1...v1.2.2) (2021-05-17)


### Bug Fixes

* debug logging ([af0ce23](http://bitbucket.org/thermsio/rpc-client-ts/commits/af0ce2395f6e9c27f4c1653cb58fbe1129e1f0aa))
* postinstall script ([429b495](http://bitbucket.org/thermsio/rpc-client-ts/commits/429b49563acdd7e958cf7de2fcb1d36c2216f89a))
* **InMemoryCache:** json string/parse before get/set ([b7cb925](http://bitbucket.org/thermsio/rpc-client-ts/commits/b7cb925633873db34c874a910911ba23f738c057))

## [1.2.1](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.2.0...v1.2.1) (2021-05-16)


### Bug Fixes

* **WebSocketTransport:** delete pending request promises after finished ([ebe3cef](http://bitbucket.org/thermsio/rpc-client-ts/commits/ebe3cef1440a4e1ca42b7321fede6e888f02b710))

# [1.2.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.1.0...v1.2.0) (2021-05-10)


### Features

* expose websocket connetion status changes on RPCClient ([67f4d86](http://bitbucket.org/thermsio/rpc-client-ts/commits/67f4d86b5437eaf5aa90835bd6bfff81fdec3a68))

# [1.1.0](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.13...v1.1.0) (2021-05-04)


### Features

* **RPCClient:** call will throw when success == false ([6c29f94](http://bitbucket.org/thermsio/rpc-client-ts/commits/6c29f9420f8e2e9d06b9b4671cc8c002f518caac))

## [1.0.13](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.12...v1.0.13) (2021-04-27)


### Bug Fixes

* **WebSocket:** resetConnection logic ([0b51ab3](http://bitbucket.org/thermsio/rpc-client-ts/commits/0b51ab3916dea074d7673d1c2190ca77a214fb4d))

## [1.0.12](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.11...v1.0.12) (2021-04-27)


### Bug Fixes

* websocket set identity logic ([8a8bb01](http://bitbucket.org/thermsio/rpc-client-ts/commits/8a8bb0174619582cc6b21289634785a48557044e))

## [1.0.11](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.10...v1.0.11) (2021-04-26)


### Bug Fixes

* debug logs in CallManager ([75d8ac8](http://bitbucket.org/thermsio/rpc-client-ts/commits/75d8ac88bf08f2b2a4eecfa80cb1ca63b546503f))

## [1.0.10](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.9...v1.0.10) (2021-04-26)


### Bug Fixes

* bundle with node-polyfills for debug pkg ([04a85b0](http://bitbucket.org/thermsio/rpc-client-ts/commits/04a85b09f22126131b12b1a306773792d18d86eb))

## [1.0.9](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.8...v1.0.9) (2021-04-26)


### Bug Fixes

* add debug logs ([a01bfdd](http://bitbucket.org/thermsio/rpc-client-ts/commits/a01bfddcbc018df75463306ffc54dd167cb46f4d))
* setIdentity and add tests ([4e68082](http://bitbucket.org/thermsio/rpc-client-ts/commits/4e68082bc0ef0ff76373590c684026b0d02496cd))

## [1.0.8](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.7...v1.0.8) (2021-04-26)


### Bug Fixes

* **WebSocketTransport:** set identity fixes ([7a545e9](http://bitbucket.org/thermsio/rpc-client-ts/commits/7a545e9ac8dcb6f7f0506d2df9e12116d210946b))

## [1.0.7](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.6...v1.0.7) (2021-04-26)


### Bug Fixes

* update deps ([de32df7](http://bitbucket.org/thermsio/rpc-client-ts/commits/de32df75c2b5050e37a79259a166574ebee04d12))

## [1.0.6](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.5...v1.0.6) (2021-04-26)


### Bug Fixes

* remove atomic-sleep ([cbeea27](http://bitbucket.org/thermsio/rpc-client-ts/commits/cbeea27761278c66449708be1672142fb3b1b283))

## [1.0.5](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.4...v1.0.5) (2021-04-26)


### Bug Fixes

* remove atomic-sleep ([c20972c](http://bitbucket.org/thermsio/rpc-client-ts/commits/c20972c99c305f890de357e4b22f241b3cb7508e))

## [1.0.4](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.3...v1.0.4) (2021-04-26)


### Bug Fixes

* rename type files removing .d.ts ([2f4b4d9](http://bitbucket.org/thermsio/rpc-client-ts/commits/2f4b4d9786a310ced8fe024d781731c0a646ed76))

## [1.0.3](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.2...v1.0.3) (2021-04-01)


### Bug Fixes

* CallManager transport delegation while loop ([7c66018](http://bitbucket.org/thermsio/rpc-client-ts/commits/7c6601840df2dfa7a1429737c1ea8f351f5b1168))

## [1.0.2](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.1...v1.0.2) (2021-03-24)


### Bug Fixes

* npmignore ([88b23b7](http://bitbucket.org/thermsio/rpc-client-ts/commits/88b23b7d21bda37a2a7a10c51d3443460378b6a0))

## [1.0.1](http://bitbucket.org/thermsio/rpc-client-ts/compare/v1.0.0...v1.0.1) (2021-03-24)


### Bug Fixes

* build script ([707cc90](http://bitbucket.org/thermsio/rpc-client-ts/commits/707cc90615db0123f2f2492632e67fd3b8379834))

# 1.0.0 (2021-03-22)


### Bug Fixes

* build ([d36df8a](http://bitbucket.org/thermsio/rpc-client-ts/commits/d36df8ac602774a4ed3857c6ad8ba416db5226bd))
* CallResponseDTO typo ([6a8ee01](http://bitbucket.org/thermsio/rpc-client-ts/commits/6a8ee0176ad5af1762dfe090945d8728e4d5489e))
* deps for build ([498e081](http://bitbucket.org/thermsio/rpc-client-ts/commits/498e0810cb5a39f413e410f085e5aad8a24a9762))
* jest tests and tsconfig changed ([dfd951b](http://bitbucket.org/thermsio/rpc-client-ts/commits/dfd951bd39cf5f613bf5b4d7cd38dd3d107bae11))
* request and response interceptors ([29b60d3](http://bitbucket.org/thermsio/rpc-client-ts/commits/29b60d32c9de21ad8333a6fd78d787d83bf0241d))
* tests ([63efa54](http://bitbucket.org/thermsio/rpc-client-ts/commits/63efa54ecf6cbe62331164d06843cd4156c6f778))


### Features

* basic server w/ http and tests working ([caeddc8](http://bitbucket.org/thermsio/rpc-client-ts/commits/caeddc8f3ebe797c436cab70c9657b5b5e979129))
* HTTPTransport logic with fetch ([002692d](http://bitbucket.org/thermsio/rpc-client-ts/commits/002692d376264278b3dd12c03f76dbcf2589217d))
* setIdentity RPCClient ([7757fe6](http://bitbucket.org/thermsio/rpc-client-ts/commits/7757fe650e65988a8fc92ed42dbaccd0e7c74980))
* **Client:** ProcedureCall methods implemented w/ passing tests ([04887d2](http://bitbucket.org/thermsio/rpc-client-ts/commits/04887d2bcf7a62345771c3b509ee78a98c318c3f))
