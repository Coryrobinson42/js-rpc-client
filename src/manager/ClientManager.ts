import { Cache } from '../cache/Cache'
import { Transport } from '../transports/Transport'
import { CallRequestDTO } from '../CallRequestDTO'
import { CallResponseDTO } from '../CallResponseDTO'
import { CallRequestError } from '../errors/CallRequestError'
import { CallRequestTransportError } from '../transports/errors/CallRequestTransportError'
import { ManagerCallRequestTimeoutError } from './errors/ManagerCallRequestTimeoutError'
import { CallResponseInterceptor } from '../CallResponseInterceptor'
import { CallRequestInterceptor } from '../CallRequestInterceptor'
import { makeCallRequestKey } from '../cache/utils/cache-utils'
import { RPCClientIdentity } from '../RPCClientIdentity'
import { cloneDeep } from 'lodash-es'
import { GetDebugLogger } from '../utils/debug-logger'

const debug = GetDebugLogger('rpc:ClientManager')

interface ClientManagerOptions {
  cache?: Cache
  deadlineMs: number
  requestInterceptor?: CallRequestInterceptor
  responseInterceptor?: CallResponseInterceptor
  transports: Transport[]
}

export interface ClientManager {
  addResponseInterceptor(interceptor: CallResponseInterceptor): void
  addRequestInterceptor(interceptor: CallRequestInterceptor): void
  clearCache(request?: CallRequestDTO): void
  getCachedResponse(request: CallRequestDTO): CallResponseDTO | undefined
  getInFlightCallCount(): number
  manageClientRequest(request: CallRequestDTO): Promise<CallResponseDTO>
  setIdentity(identity?: RPCClientIdentity): void
}

export class ClientManager implements ClientManager {
  private cache?: Cache
  private inflightCallsByKey: { [key: string]: Promise<CallResponseDTO> } = {}
  private interceptors: {
    request: CallRequestInterceptor[]
    response: CallResponseInterceptor[]
  } = {
    request: [],
    response: [],
  }
  private transports: Transport[]

  constructor(readonly options: ClientManagerOptions) {
    this.cache = options.cache

    if (options.requestInterceptor) {
      this.interceptors.request.push(options.requestInterceptor)
    }

    if (options.responseInterceptor) {
      this.interceptors.response.push(options.responseInterceptor)
    }

    this.transports = options.transports
  }

  public addResponseInterceptor = (interceptor: CallResponseInterceptor) => {
    if (typeof interceptor !== 'function')
      throw new Error('cannot add interceptor that is not a function')
    this.interceptors.response.push(interceptor)
  }

  public addRequestInterceptor = (interceptor: CallRequestInterceptor) => {
    if (typeof interceptor !== 'function')
      throw new Error('cannot add interceptor that is not a function')
    this.interceptors.request.push(interceptor)
  }

  public clearCache = (request?: CallRequestDTO) => {
    debug('clearCache')

    if (!this.cache) return

    if (request) {
      this.cache.setCachedResponse(request, undefined)
    } else {
      this.cache.clearCache()
    }
  }

  public getCachedResponse = (request: CallRequestDTO) => {
    debug('getCachedResponse', request)

    return this?.cache?.getCachedResponse(request)
  }

  public getInFlightCallCount = () => {
    debug('getInFlightCallCount')

    return Object.keys(this.inflightCallsByKey).length
  }

  public manageClientRequest = async (originalRequest: CallRequestDTO) => {
    debug('manageClientRequest', originalRequest)

    // todo: wrap w/ try/catch and throw interceptor failed error
    const request = await this.interceptRequestMutator(originalRequest)

    const key = makeCallRequestKey(request)

    if (this.inflightCallsByKey.hasOwnProperty(key)) {
      debug('manageClientRequest using an existing in-flight call', key)

      return this.inflightCallsByKey[key].then((res: CallResponseDTO) => {
        const response = cloneDeep(res)

        if (originalRequest.correlationId) {
          response.correlationId = originalRequest.correlationId
        }

        return response
      })
    }

    const requestPromiseWrapper = async () => {
      const transportResponse = await this.sendRequestWithTransport(request)

      // todo: wrap w/ try/catch and throw interceptor failed error
      const response = await this.interceptResponseMutator(
        transportResponse,
        request,
      )

      if (this.cache && response) {
        this.cache.setCachedResponse(request, response)
      }

      return response
    }

    const requestPromise = requestPromiseWrapper()
      .then((response) => {
        delete this.inflightCallsByKey[key]

        return response
      })
      .catch((err) => {
        delete this.inflightCallsByKey[key]

        throw err
      })

    this.inflightCallsByKey[key] = requestPromise

    return await requestPromise
  }

  public setIdentity = (identity?: RPCClientIdentity) => {
    debug('setIdentity', identity)

    this.transports.forEach((transport) => transport.setIdentity(identity))
  }

  private interceptRequestMutator = async (
    originalRequest: CallRequestDTO,
  ): Promise<CallRequestDTO> => {
    let request = originalRequest

    if (this.interceptors.request.length) {
      debug('interceptRequestMutator(), original request:', originalRequest)

      for (const interceptor of this.interceptors.request) {
        request = await interceptor(request)
      }
    }

    return request
  }

  private interceptResponseMutator = async (
    originalResponse: CallResponseDTO,
    request: CallRequestDTO,
  ): Promise<CallResponseDTO> => {
    let response = originalResponse

    if (this.interceptors.response.length) {
      debug('interceptResponseMutator', request, originalResponse)

      for (const interceptor of this.interceptors.response) {
        try {
          response = await interceptor(response, request)
        } catch (e) {
          debug(
            'caught response interceptor, request:',
            request,
            'original response:',
            originalResponse,
            'mutated response:',
            response,
          )

          throw e
        }
      }
    }

    return response
  }

  private sendRequestWithTransport = async (request: CallRequestDTO) => {
    debug('sendRequestWithTransport', request)

    let response
    let transportsIndex = 0

    while (!response && this.transports[transportsIndex]) {
      const transport = this.transports[transportsIndex]

      if (!transport.isConnected()) {
        transportsIndex++
        continue
      }

      debug(
        `sendRequestWithTransport trying ${this.transports[transportsIndex].name}`,
        request,
      )

      try {
        let timeout: any

        const deadlinePromise = new Promise((_, reject) => {
          timeout = setTimeout(() => {
            reject(
              new ManagerCallRequestTimeoutError(
                `Procedure ${request.procedure}`,
              ),
            )
          }, this.options.deadlineMs)
        })

        response = await Promise.race([
          deadlinePromise,
          transport.sendRequest(request),
        ]).then((response) => {
          clearTimeout(timeout)

          // the timeout will reject, CallResponseDTO is the only response that will be passed
          return response as CallResponseDTO
        })
      } catch (e: any) {
        if (
          e instanceof ManagerCallRequestTimeoutError ||
          e instanceof CallRequestTransportError
        ) {
          debug(
            `sending request with ${this.transports[transportsIndex].name} failed, trying next transport`,
          )
          transportsIndex++
        } else {
          throw e
        }
      }
    }

    if (!response && !this.transports[transportsIndex]) {
      throw new CallRequestError(
        `Procedure ${request.procedure} did not get a response from any transports`,
      )
    } else if (!response) {
      throw new CallRequestError(
        `Procedure ${request.procedure} did not get a response from the remote`,
      )
    }

    return response
  }
}
