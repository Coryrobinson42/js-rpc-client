import { CallRequestDTO } from '../CallRequestDTO'
import { CallResponseDTO } from '../CallResponseDTO'
import { RPCClientIdentity } from '../RPCClientIdentity'

export interface Transport {
  isConnected(): boolean

  name: string

  sendRequest(call: CallRequestDTO): Promise<CallResponseDTO>

  setIdentity(identity?: RPCClientIdentity): void
}
