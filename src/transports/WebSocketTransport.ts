import { Transport } from './Transport'
import { CallRequestDTO } from '../CallRequestDTO'
import { CallResponseDTO } from '../CallResponseDTO'
import { RPCClientIdentity } from '../RPCClientIdentity'
import { PromiseWrapper } from '../utils/PromiseWrapper'
import { sleep } from '../utils/sleep'
import { GetDebugLogger } from '../utils/debug-logger'
import { CallRequestTransportError } from './errors/CallRequestTransportError'

const debug = GetDebugLogger('rpc:WebSocketTransport')

interface WebSocketTransportOptions {
  host: string
  identity?: RPCClientIdentity
  onConnectionStatusChange?: (connected: boolean) => void
}

export class WebSocketTransport implements Transport {
  static DEFAULT_TIMEOUT_MS = 10000

  private connectedToRemote: boolean = false
  private readonly host: string
  private identity?: RPCClientIdentity
  private isWaitingForIdentityConfirmation: boolean = false
  // private inFlightCallCount: number = 0;
  private pendingPromisesForResponse: {
    [correlationId: string]: PromiseWrapper<
      CallResponseDTO,
      CallResponseDTO | CallRequestTransportError
    >
  } = {}
  private websocket?: WebSocket
  // we keep a connection ID in case the ws connection changes mid-flight for requests
  private websocketId?: number = undefined

  readonly name = 'WebSocketTransport'

  constructor(readonly options: WebSocketTransportOptions) {
    debug('new WebSocketTransport()')
    this.host = options.host

    this.connect()
  }

  public isConnected = () => {
    return this.connectedToRemote && !!this.websocket
  }

  public sendRequest = async (
    call: CallRequestDTO,
  ): Promise<CallResponseDTO> => {
    debug('sendRequest', call)

    while (this.isWaitingForIdentityConfirmation) {
      debug('waiting for identity confirmation')

      await sleep(100)
    }

    return this.send(call)
  }

  /**
   * Set the identity of this WS connection with the server.
   * @param identity {RPCClientIdentity} If no identity passed, this method will reset/clear the identity
   */
  public setIdentity = (identity?: RPCClientIdentity) => {
    debug('setIdentity', identity)

    if (!identity) {
      this.identity = undefined
      this.resetConnection()

      return
    } else {
      this.identity = identity
    }

    if (!this.connectedToRemote) {
      debug('setIdentity is not connected to remote')
      return
    }

    this.isWaitingForIdentityConfirmation = true

    this.send({ identity })
      .then(() => {
        debug('setIdentity with remote complete')
      })
      .catch((e) => {
        debug('setIdentity with remote error', e)
      })
      .finally(() => {
        this.isWaitingForIdentityConfirmation = false
      })
  }

  private connect = () => {
    debug('connect', this.host)

    if (this.websocket) {
      debug('connect() returning early, websocket already exists')
      return
    }

    this.websocketId = Math.random()
    this.websocket = new WebSocket(this.host)

    const ws = this.websocket

    ws.onopen = () => {
      console.log('WebSocket connected:')

      this.setConnectedToRemote(true)

      if (this.identity) {
        this.setIdentity(this.identity)
      }
    }

    ws.onmessage = (msg) => {
      this.handleWebSocketMsg(msg)
    }

    ws.onclose = (e) => {
      if (this.connectedToRemote) {
        console.log('WebSocket closed', e.reason)
      } else {
        debug('WebSocket closed, it was not connected to the remote')
      }

      this.setConnectedToRemote(false)
      this.isWaitingForIdentityConfirmation = false
      this.websocket = undefined
      this.websocketId = undefined

      Object.entries(this.pendingPromisesForResponse).forEach(
        ([correlationId, promiseWrapper]) => {
          promiseWrapper.reject(
            new CallRequestTransportError('Websocket disconnected'),
          )
        },
      )

      if (!e.wasClean) {
        setTimeout(() => {
          this.connect()
        }, 1000)
      }
    }

    ws.onerror = (err) => {
      if (this.connectedToRemote) {
        console.error('Socket encountered error: ', err)
      } else {
        debug('WebSocket errored, it was not connected to the remote')
      }

      ws.close()
    }
  }

  private disconnect = () => {
    debug('disconnect')
    this.setConnectedToRemote(false)
    this.isWaitingForIdentityConfirmation = false
    this.websocket?.close()
    this.websocket = undefined
    this.websocketId = undefined
  }

  private handleWebSocketMsg = (msg: MessageEvent) => {
    debug('handleWebSocketMsg', msg)

    let json

    try {
      json = JSON.parse(msg.data)
    } catch (e: any) {
      console.error('error parsing WS msg', e)
    }

    const callResponseDTO = new CallResponseDTO(json)

    if (!callResponseDTO.correlationId) {
      console.error(
        'RPCClient WebSocketTransport received unexpected msg from the server, not correlationId found in response.',
        json,
      )

      return
    }

    const pendingRequestPromise =
      this.pendingPromisesForResponse[callResponseDTO.correlationId]

    if (pendingRequestPromise) {
      pendingRequestPromise.resolve(callResponseDTO)
    } else {
      console.warn(
        "rcvd WS msg/response that doesn't match any pending RPC's",
        json,
      )
    }
  }

  private resetConnection = () => {
    debug('resetConnection')

    this.disconnect()
    this.connect()
  }

  private send = async (call: CallRequestDTO): Promise<CallResponseDTO> => {
    debug('send', call)

    const correlationId = Math.random().toString()

    call.correlationId = call.correlationId || correlationId

    let promiseWrapper = new PromiseWrapper<
      CallResponseDTO,
      CallResponseDTO | CallRequestTransportError
    >(WebSocketTransport.DEFAULT_TIMEOUT_MS)

    this.websocket?.send(JSON.stringify(call))

    this.pendingPromisesForResponse[call.correlationId] = promiseWrapper

    return await promiseWrapper.promise.finally(() => {
      delete this.pendingPromisesForResponse[call.correlationId!]
    })
  }

  private setConnectedToRemote = (connected: boolean) => {
    this.connectedToRemote = connected

    if (this.options.onConnectionStatusChange) {
      this.options.onConnectionStatusChange(connected)
    }
  }
}
