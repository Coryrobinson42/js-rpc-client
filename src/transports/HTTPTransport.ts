import { Transport } from './Transport'
import { CallRequestDTO } from '../CallRequestDTO'
import { CallResponseDTO } from '../CallResponseDTO'
import { RPCClientIdentity } from '../RPCClientIdentity'
import { GetDebugLogger } from '../utils/debug-logger'

const debug = GetDebugLogger('rpc:HTTPTransport')

interface HTTPTransportOptions {
  host: string
}

export class HTTPTransport implements Transport {
  private readonly host: string
  private identity?: RPCClientIdentity
  // private inFlightCallCount: number = 0;
  private networkIsConnected: boolean = false

  readonly name = 'HttpTransport'

  constructor(readonly options: HTTPTransportOptions) {
    this.host = options.host
  }

  public isConnected = (): boolean => {
    if (typeof window !== 'undefined') {
      return !!window?.navigator?.onLine
    }

    return this.networkIsConnected
  }

  public sendRequest = async (
    call: CallRequestDTO,
  ): Promise<CallResponseDTO> => {
    debug('sendRequest', call)

    const body = JSON.stringify({
      identity: this.identity,
      ...call,
    })

    const res = await window?.fetch(this.host, {
      body,
      cache: 'default',
      credentials: 'omit',
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'POST',
      mode: 'cors',
      redirect: 'follow',
      referrerPolicy: 'origin',
    })

    const string = await res.text()

    if (!string) throw new Error('No response received from remote')

    const response = JSON.parse(string)

    return new CallResponseDTO(response)
  }

  public setIdentity = (identity?: RPCClientIdentity) => {
    debug('setIdentity', identity)

    this.identity = identity
  }
}
