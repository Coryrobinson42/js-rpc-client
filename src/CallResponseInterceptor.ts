import { CallRequestDTO } from './CallRequestDTO'
import { CallResponseDTO } from './CallResponseDTO'

export type CallResponseInterceptor = (
  response: CallResponseDTO,
  request: CallRequestDTO,
) => Promise<CallResponseDTO> | CallResponseDTO
