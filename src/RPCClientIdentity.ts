/**
 * Identity is the client's Identity, ie: User/auth/device/etc.
 */
export interface RPCClientIdentity {
  authorization?: string
  deviceName?: string
  metadata?: { [key: string]: any }
}
