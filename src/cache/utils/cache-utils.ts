import { CallRequestDTO } from '../../CallRequestDTO'
import stringify from 'fast-json-stable-stringify'
import sortKeys from '../../utils/sort-keys'

export const makeCallRequestKey = (request: CallRequestDTO): string => {
  const strippedCallRequest = {
    args: request.args,
    procedure: request.procedure,
    scope: request.scope,
    version: request.version,
  }

  let key

  // if Array or Object we want a deterministic key
  if (
    Array.isArray(strippedCallRequest) ||
    typeof strippedCallRequest === 'object'
  ) {
    key = stringify(sortKeys(strippedCallRequest, { deep: true }))
  } else {
    console.warn(
      'makeCallRequestKey() was not passed a CallRequestDTO',
      request,
    )

    throw new Error('makeCallRequestKey() was not passed a CallRequestDTO')
  }

  return key
}
