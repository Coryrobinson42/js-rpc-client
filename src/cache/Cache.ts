import { CallResponseDTO } from '../CallResponseDTO'
import { CallRequestDTO } from '../CallRequestDTO'

export interface Cache {
  clearCache(): void
  getCachedResponse(request: CallRequestDTO): CallResponseDTO | undefined
  setCachedResponse(request: CallRequestDTO, response?: CallResponseDTO): void
}
