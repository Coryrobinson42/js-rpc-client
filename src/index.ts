export { RPCClient, RPCClientOptions } from './RPCClient'

import { CallRequestDTO } from './CallRequestDTO'
export type RPCRequest = CallRequestDTO | string

export { CallResponseDTO as RPCResponse } from './CallResponseDTO'
export { RPCClientIdentity } from './RPCClientIdentity'

// todo: @deprecated - remove this export in next major release
export { RPCClientIdentity as RPCIdentity } from './RPCClientIdentity'
