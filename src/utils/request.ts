import {
  DEFAULT_REQUEST_SCOPE,
  DEFAULT_REQUEST_VERSION,
  CallRequestDTO,
} from '../CallRequestDTO'

export const parseRequestShorthand = (
  requestString: string,
): CallRequestDTO => {
  const parsed = requestString.split('::')

  return {
    procedure: parsed[1],
    scope: parsed[0] || DEFAULT_REQUEST_SCOPE,
    version: parsed[2] || DEFAULT_REQUEST_VERSION,
  }
}
