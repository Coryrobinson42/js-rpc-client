import { CallRequestDTO } from './CallRequestDTO'
import { CallResponseDTO } from './CallResponseDTO'
import { ClientManager } from './manager/ClientManager'
import { BrowserCache } from './cache/BrowserStorageCache'
import { InMemoryCache } from './cache/InMemoryCache'
import { HTTPTransport } from './transports/HTTPTransport'
import { WebSocketTransport } from './transports/WebSocketTransport'
import { CallResponseInterceptor } from './CallResponseInterceptor'
import { CallRequestInterceptor } from './CallRequestInterceptor'
import { Transport } from './transports/Transport'
import { RPCClientIdentity } from './RPCClientIdentity'
import { cloneDeep, merge } from 'lodash-es'
import { parseRequestShorthand } from './utils/request'

const ERRORS = {
  HTTP_HOST_OR_TRANSPORT_REQUIRED: `http host or tansport is required`,
  INTERCEPTOR_MUSTBE_FUNC: `interceptors must be a function`,
}

export interface RPCClient {
  call<Args = any, Data = any>(
    request: CallRequestDTO<Args> | string,
    args?: Args,
  ): Promise<CallResponseDTO<Data>>

  clearCache(request?: CallRequestDTO): void

  getCallCache<Args = any, Data = any>(
    request: CallRequestDTO<Args> | string,
    args?: Args,
  ): CallResponseDTO<Data> | undefined

  getIdentity(): RPCClientIdentity | undefined

  getInFlightCallCount(): number

  getWebSocketConnected(): boolean

  makeProcedure<Args = any, Data = any>(
    request: CallRequestDTO<Args> | string,
  ): (args?: any) => Promise<CallResponseDTO<Data>>

  registerResponseInterceptor(
    responseInterceptor: CallResponseInterceptor,
  ): void
  registerRequestInterceptor(requestInterceptor: CallRequestInterceptor): void

  setIdentity(identity?: RPCClientIdentity): void
}

export interface RPCClientOptions {
  cacheType?: 'browser' | 'memory'
  cacheMaxAgeMs?: number
  deadlineMs?: number
  hosts?: {
    // if http host URI provided, will use default HTTPTransport
    http?: string
    // if ws host URI provided, will use default WebSocketTransport
    websocket?: string
  }
  onWebSocketConnectionStatusChange?: (connected: boolean) => void
  requestInterceptor?: CallRequestInterceptor
  responseInterceptor?: CallResponseInterceptor
  transports?: {
    // if http transport provided, will use over the default Transport
    http?: Transport
    // if ws transport provided, will use over the default Transport
    websocket?: Transport
  }
}

export class RPCClient implements RPCClient {
  static DEFAULT_DEADLINE_MS = 10000

  private callManager: ClientManager
  private identity: RPCClientIdentity | undefined
  private readonly httpTransport?: HTTPTransport
  private readonly webSocketTransport?: WebSocketTransport

  constructor(readonly options: RPCClientOptions) {
    if (!options?.hosts?.http && !options?.transports?.http) {
      throw new Error(ERRORS.HTTP_HOST_OR_TRANSPORT_REQUIRED)
    }

    if (
      options.requestInterceptor &&
      typeof options.requestInterceptor !== 'function'
    ) {
      throw new Error(ERRORS.INTERCEPTOR_MUSTBE_FUNC)
    }

    if (
      options.responseInterceptor &&
      typeof options.responseInterceptor !== 'function'
    ) {
      throw new Error(ERRORS.INTERCEPTOR_MUSTBE_FUNC)
    }

    let cache
    const cacheOptions = { cacheMaxAgeMs: options?.cacheMaxAgeMs }

    if (options.cacheType == 'browser') {
      cache = new BrowserCache(cacheOptions)
    } else {
      cache = new InMemoryCache(cacheOptions)
    }

    const transports = []

    if (options.transports?.websocket) {
      transports.push(options.transports.websocket)
    } else if (options?.hosts?.websocket) {
      this.webSocketTransport = new WebSocketTransport({
        host: options.hosts.websocket,
        onConnectionStatusChange: options.onWebSocketConnectionStatusChange,
      })

      transports.push(this.webSocketTransport)
    }

    // put HTTP last because transports list determines the order of use
    if (options.transports?.http) {
      transports.push(options.transports.http)
    } else if (options?.hosts?.http) {
      this.httpTransport = new HTTPTransport({ host: options.hosts.http })

      transports.push(this.httpTransport)
    }

    this.callManager = new ClientManager({
      cache,
      deadlineMs: options.deadlineMs || RPCClient.DEFAULT_DEADLINE_MS,
      requestInterceptor: options.requestInterceptor,
      responseInterceptor: options.responseInterceptor,
      transports,
    })
  }

  public call = async <Args = any, Data = any>(
    request: CallRequestDTO<Args> | string,
    args?: Args,
  ): Promise<CallResponseDTO<Data>> => {
    if (!request)
      throw new Error('RPCClient.call(request) requires a "request" param')

    let req: CallRequestDTO<Args>

    if (typeof request === 'string') req = parseRequestShorthand(request)
    else req = request as CallRequestDTO<Args>

    if (args) {
      req.args = args
    }

    const requestDTO = new CallRequestDTO<Args>(req)

    if (!requestDTO.procedure && !requestDTO.identity) {
      throw new TypeError(
        'RPCClient#call requires a "identity" or "procedure" prop and received neither',
      )
    }

    if (!requestDTO.identity) requestDTO.identity = {}

    // don't overwrite this.identity with the requestDTO identity
    requestDTO.identity = merge({ ...this.identity }, requestDTO.identity)

    const callResponse = await this.callManager.manageClientRequest(requestDTO)

    if (!callResponse.success) {
      throw callResponse
    }

    return callResponse as CallResponseDTO<Data>
  }

  /**
   * Request is optional, if provided then only the cache for that request is cleared
   * @param request {CallRequestDTO}
   */
  public clearCache = (request?: CallRequestDTO) => {
    this.callManager.clearCache(request)
  }

  public getCallCache = <Args = any, Data = any>(
    request: CallRequestDTO<Args> | string,
    args?: Args,
  ) => {
    let req: CallRequestDTO<Args>

    if (typeof request === 'string') req = parseRequestShorthand(request)
    else req = request as CallRequestDTO<Args>

    if (args) {
      req.args = args
    }

    const cachedResponse = this.callManager.getCachedResponse(
      req,
    ) as CallResponseDTO<Data>

    if (cachedResponse) return cachedResponse

    return undefined
  }

  public getIdentity = () =>
    this.identity ? cloneDeep(this.identity) : this.identity

  public getInFlightCallCount = () => {
    return this.callManager.getInFlightCallCount()
  }

  public getWebSocketConnected = () => {
    return !!this?.webSocketTransport?.isConnected()
  }

  public makeProcedure = <Args = any, Data = any>(
    request: CallRequestDTO<Args> | string,
  ): ((args?: any) => Promise<CallResponseDTO<Data>>) => {
    const self = this

    let req: CallRequestDTO<Args>

    if (typeof request === 'string') req = parseRequestShorthand(request)
    else req = request as CallRequestDTO<Args>

    return function curriedProcedure(args?: any) {
      return self.call({ ...req, args })
    }
  }

  public registerResponseInterceptor = (
    responseInterceptor: CallResponseInterceptor,
  ) => {
    this.callManager.addResponseInterceptor(responseInterceptor)
  }

  public registerRequestInterceptor = (
    requestInterceptor: CallRequestInterceptor,
  ) => {
    this.callManager.addRequestInterceptor(requestInterceptor)
  }

  public setIdentity = (identity?: RPCClientIdentity) => {
    this.identity = identity
    this.callManager.setIdentity(identity)
  }
}
