export interface CallResponseDTO<Data = any> {
  code: number
  correlationId?: string
  data: Data
  message?: string
  success: boolean
}

export class CallResponseDTO<Data = any> {
  code: number
  correlationId?: string
  data: Data
  message?: string
  success: boolean

  constructor(response: CallResponseDTO<Data>) {
    const { code, correlationId, data, message, success } = response

    if (typeof code !== 'number') throw new Error('code must be a number')
    this.code = code

    if (correlationId && typeof correlationId !== 'string')
      throw new Error('correlationId must be a string')
    this.correlationId = correlationId

    this.data = data

    if (message && typeof message !== 'string')
      throw new Error('message must be a string')
    this.message = message

    if (typeof success !== 'boolean')
      throw new Error('success must be a boolean')
    this.success = success
  }
}
