test("emit rpc event", () => {
  const rpc = {
    emit: (topic: any) => void 0,
  };

  rpc.emit({
    channel: "auth", // if no channel then it's global event space
    event: "user_logged_in",
    payload: { email: "cory@therms.io" },
    version: "1",
  });
});
