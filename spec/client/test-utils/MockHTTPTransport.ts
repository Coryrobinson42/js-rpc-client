import { Transport } from '../../../src/transports/Transport'
import { CallRequestDTO } from '../../../src/CallRequestDTO'
import { CallResponseDTO } from '../../../src/CallResponseDTO'
import { RPCClientIdentity } from '../../../src/RPCClientIdentity'

export class MockHTTPTransport implements Transport {
  private identity: RPCClientIdentity | undefined

  name = 'MockHTTPTransport'

  isConnected = (): boolean => {
    return true
  }

  sendRequest = async (request: CallRequestDTO): Promise<CallResponseDTO> => {
    // specific for testing to delay response
    if (request?.args?.delayCallForTesting) {
      return new Promise((r) => {
        setTimeout(() => {
          r({
            code: 200,
            data: request.args,
            success: true,
          })
        }, request?.args?.delayCallForTesting)
      })
    }

    // specific for testing to check identity
    if (request?.args?.returnIdentity) {
      return {
        code: 200,
        data: request.identity,
        success: true,
      }
    }

    // specific for testing
    if (request?.args?.fail) {
      return {
        code: 422,
        data: undefined,
        success: false,
      }
    }

    // regular response
    return Promise.resolve({
      code: 200,
      data: request.args,
      success: true,
    })
  }

  setIdentity = (identity?: RPCClientIdentity) => {
    this.identity = identity
  }
}
