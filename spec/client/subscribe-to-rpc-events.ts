test("subscribe to rpc events", () => {
  const rpc = {
    subscribe: (topic: any) => ({
      onEvent(handler: (response: any) => void) {},

      cancelSubscription() {},
    }),
  };

  const loginEventSubscription = rpc.subscribe({
    channel: "auth", // if no channel then it's global event scope
    procedure: "user_logged_in",
    version: "1",
  });

  loginEventSubscription.onEvent((event) => {
      loginEventSubscription.cancelSubscription();
  });
});
