"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var src_1 = require("../../src");
var MockHTTPTransport_1 = require("./test-utils/MockHTTPTransport");
describe('client interceptors', function () {
    it('can register request & response interceptors', function () { return __awaiter(void 0, void 0, void 0, function () {
        var rpc, data;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    rpc = new src_1.RPCClient({
                        responseInterceptor: function (response) {
                            response.data.response = 'response_intercepted';
                            return response;
                        },
                        requestInterceptor: function (request) {
                            request.args.request = 'request_intercepted';
                            return request;
                        },
                        transports: {
                            http: new MockHTTPTransport_1.MockHTTPTransport(),
                        },
                    });
                    return [4 /*yield*/, rpc.call('test::any-call', {
                            original_data: '123',
                            response: undefined,
                            request: undefined,
                        })];
                case 1:
                    data = (_a.sent()).data;
                    expect(data.original_data).toEqual('123');
                    expect(data.response).toEqual('response_intercepted');
                    expect(data.request).toEqual('request_intercepted');
                    return [2 /*return*/];
            }
        });
    }); });
    it('can throw when request & response interceptors throws/error', function () { return __awaiter(void 0, void 0, void 0, function () {
        var rpc;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    rpc = new src_1.RPCClient({
                        responseInterceptor: function (response) {
                            // @ts-ignore
                            response.propDoesNotExist.propDoesNotExist = true;
                            return response;
                        },
                        requestInterceptor: function (request) {
                            // @ts-ignore
                            request.propDoesNotExist.propDoesNotExist = true;
                            return request;
                        },
                        transports: {
                            http: new MockHTTPTransport_1.MockHTTPTransport(),
                        },
                    });
                    return [4 /*yield*/, expect(function () {
                            return rpc.call('test::any-call', {
                                response: undefined,
                                request: undefined,
                            });
                        }).rejects.toThrow()];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }); });
    it('can register additional request & response interceptors', function () { return __awaiter(void 0, void 0, void 0, function () {
        var rpc, data;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    rpc = new src_1.RPCClient({
                        responseInterceptor: function (response) {
                            response.data.response = 'response_intercepted';
                            return response;
                        },
                        requestInterceptor: function (request) {
                            request.args.request = 'request_intercepted';
                            return request;
                        },
                        transports: {
                            http: new MockHTTPTransport_1.MockHTTPTransport(),
                        },
                    });
                    rpc.registerResponseInterceptor(function (response) {
                        response.data.response2 = 'res2 added';
                        return response;
                    });
                    rpc.registerRequestInterceptor(function (request) {
                        request.args.request2 = 'req2 added';
                        return request;
                    });
                    return [4 /*yield*/, rpc.call('test::any-call', {
                            response: undefined,
                            request: undefined,
                        })];
                case 1:
                    data = (_a.sent()).data;
                    expect(data.response).toEqual('response_intercepted');
                    expect(data.response2).toEqual('res2 added');
                    expect(data.request).toEqual('request_intercepted');
                    expect(data.request2).toEqual('req2 added');
                    return [2 /*return*/];
            }
        });
    }); });
    it('should call interceptors when there is unsuccesful responses', function () { return __awaiter(void 0, void 0, void 0, function () {
        var rpc, response, e_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    rpc = new src_1.RPCClient({
                        responseInterceptor: function (response) {
                            if (!response.data)
                                response.data = {};
                            response.data.interceptor1 = true;
                            return response;
                        },
                        transports: {
                            http: new MockHTTPTransport_1.MockHTTPTransport(),
                        },
                    });
                    rpc.registerResponseInterceptor(function (response) {
                        response.data.interceptor2 = true;
                        return response;
                    });
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, , 4]);
                    return [4 /*yield*/, rpc.call('test::any-call', {
                            fail: true,
                            response: undefined,
                            request: undefined,
                        })];
                case 2:
                    response = _a.sent();
                    return [3 /*break*/, 4];
                case 3:
                    e_1 = _a.sent();
                    response = e_1;
                    return [3 /*break*/, 4];
                case 4:
                    expect(response).toBeDefined();
                    expect(response.success).toEqual(false);
                    expect(response.data.interceptor1).toBeTruthy();
                    expect(response.data.interceptor2).toBeTruthy();
                    return [2 /*return*/];
            }
        });
    }); });
});
