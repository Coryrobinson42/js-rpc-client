import { RPCClient } from '../../src'
import { CallResponseDTO } from '../../src/CallResponseDTO'
import { MockHTTPTransport } from './test-utils/MockHTTPTransport'

describe('client interceptors', () => {
  it('can register request & response interceptors', async () => {
    const rpc = new RPCClient({
      responseInterceptor: (response) => {
        response.data.response = 'response_intercepted'
        return response
      },
      requestInterceptor: (request) => {
        request.args.request = 'request_intercepted'
        return request
      },
      transports: {
        http: new MockHTTPTransport(),
      },
    })

    const { data } = await rpc.call('test::any-call', {
      original_data: '123',
      response: undefined,
      request: undefined,
    })

    expect(data.original_data).toEqual('123')
    expect(data.response).toEqual('response_intercepted')
    expect(data.request).toEqual('request_intercepted')
  })

  it('can throw when request & response interceptors throws/error', async () => {
    const rpc = new RPCClient({
      responseInterceptor: (response) => {
        // @ts-ignore
        response.propDoesNotExist.propDoesNotExist = true
        return response
      },
      requestInterceptor: (request) => {
        // @ts-ignore
        request.propDoesNotExist.propDoesNotExist = true
        return request
      },
      transports: {
        http: new MockHTTPTransport(),
      },
    })

    await expect(() =>
      rpc.call('test::any-call', {
        response: undefined,
        request: undefined,
      }),
    ).rejects.toThrow()
  })

  it('can register additional request & response interceptors', async () => {
    const rpc = new RPCClient({
      responseInterceptor: (response) => {
        response.data.response = 'response_intercepted'
        return response
      },
      requestInterceptor: (request) => {
        request.args.request = 'request_intercepted'
        return request
      },
      transports: {
        http: new MockHTTPTransport(),
      },
    })

    rpc.registerResponseInterceptor((response) => {
      response.data.response2 = 'res2 added'
      return response
    })
    rpc.registerRequestInterceptor((request) => {
      request.args.request2 = 'req2 added'
      return request
    })

    const { data } = await rpc.call('test::any-call', {
      response: undefined,
      request: undefined,
    })

    expect(data.response).toEqual('response_intercepted')
    expect(data.response2).toEqual('res2 added')
    expect(data.request).toEqual('request_intercepted')
    expect(data.request2).toEqual('req2 added')
  })

  it('should call interceptors when there is unsuccesful responses', async () => {
    const rpc = new RPCClient({
      responseInterceptor: (response) => {
        if (!response.data) response.data = {}

        response.data.interceptor1 = true

        return response
      },
      transports: {
        http: new MockHTTPTransport(),
      },
    })

    rpc.registerResponseInterceptor((response) => {
      response.data.interceptor2 = true

      return response
    })

    let response: CallResponseDTO

    try {
      response = await rpc.call('test::any-call', {
        fail: true, // send to MockHTTPTransport
        response: undefined,
        request: undefined,
      })
    } catch (e: any) {
      response = e as CallResponseDTO
    }

    expect(response).toBeDefined()
    expect(response.success).toEqual(false)
    expect(response.data.interceptor1).toBeTruthy()
    expect(response.data.interceptor2).toBeTruthy()
  })
})
