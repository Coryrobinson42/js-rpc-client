import { RPCClient } from '../../src'
import { MockHTTPTransport } from './test-utils/MockHTTPTransport'
import { CallRequestDTO } from '../../src/CallRequestDTO'
import { CallResponseDTO } from '../../src/CallResponseDTO'

describe('make rpc calls', () => {
  const mockCallArgs = {
    email: 'cory@therms.io',
    password: '123456',
  }

  const rpc = new RPCClient({
    cacheMaxAgeMs: 100,
    deadlineMs: 100,
    transports: {
      http: new MockHTTPTransport(),
    },
  })

  it('can make a procedure call', async () => {
    const { code, data, success } = await rpc.call({
      args: mockCallArgs,
      procedure: 'get-some-data',
      scope: 'some-data',
      version: '1',
    })

    expect(data).toEqual(mockCallArgs)
    expect(code).toEqual(200)
    expect(success).toEqual(true)
  })

  it('throws a CallResponse when RPC is unsuccesful', async () => {
    try {
      const { code, data, success } = await rpc.call({
        args: { fail: true },
        procedure: 'fail',
        scope: 'anything',
        version: '1',
      })
    } catch (e: any) {
      // we don't want typeof to be 'function' which is a Proxy
      expect(typeof e).toEqual('object')
      expect(e.code).toEqual(422)
      expect(e.success).toBeFalsy()
    }
  })

  // todo: to be implemented...
  // it("subscribes and received remote call updates", async (done) =pendingPromisesForResponse[callResponseDTO.correlationId]> {
  //   getSomeDataProcedure.onRemoteCallUpdate((response) => {
  //     // do somethig with the new call response data...
  //
  //     getSomeDataProcedure.cancelCallResponseUpdates();
  //
  //     // expect(getSomeDataProcedure.isSubscribedToRemoteUpdates).toBeFalsy();
  //
  //     done();
  //   });
  //
  //   expect(getSomeDataProcedure.isSubscribedToRemoteUpdates).toBeTruthy();
  // });

  it('caches responses', async () => {
    const callOptions = {
      args: mockCallArgs,
      procedure: 'get-cached-data',
      scope: 'cache',
      version: '1',
    }

    const cachedResponse1 = rpc.getCallCache(callOptions)

    expect(cachedResponse1).toBeUndefined()

    await rpc.call(callOptions)

    const cachedResponse2 = rpc.getCallCache(callOptions)

    expect(cachedResponse2).toBeDefined()
    expect(cachedResponse2?.code).toEqual(200)
    expect(cachedResponse2?.data).toEqual(mockCallArgs)
    expect(cachedResponse2?.success).toEqual(true)
  })

  it('clears specific request cache', async () => {
    const callOptions = {
      args: mockCallArgs,
      procedure: 'clear-cached-data-request',
      scope: 'cache',
      version: '1',
    }

    await rpc.call(callOptions)

    const cachedResponse1 = rpc.getCallCache(callOptions)

    expect(cachedResponse1).toBeDefined()

    rpc.clearCache(callOptions)

    const cachedResponse2 = rpc.getCallCache(callOptions)

    expect(cachedResponse2).toBeUndefined()
  })

  it('clears entire cache', async () => {
    const callOptions = {
      args: mockCallArgs,
      procedure: 'clear-cached-data',
      scope: 'cache',
      version: '1',
    }

    await rpc.call(callOptions)

    const cachedResponse1 = rpc.getCallCache(callOptions)

    expect(cachedResponse1).toBeDefined()

    rpc.clearCache()

    const cachedResponse2 = rpc.getCallCache(callOptions)

    expect(cachedResponse2).toBeUndefined()
  })

  it('auto clears cache after expire', async () => {
    const callOptions = {
      args: mockCallArgs,
      procedure: 'clear-cached-data-expire',
      scope: 'cache',
      version: '1',
    }

    await rpc.call(callOptions)

    const cachedResponse1 = rpc.getCallCache(callOptions)

    expect(cachedResponse1).toBeDefined()

    return new Promise((done) => {
      setTimeout(() => {
        const cachedResponse2 = rpc.getCallCache(callOptions)

        expect(cachedResponse2).toBeUndefined()

        done(true)
      }, 150)
    })
  })

  it('enforces a call deadline by throwing', async () => {
    const callOptions = {
      args: { delayCallForTesting: 150 },
      procedure: 'call-deadline',
      scope: 'cache',
      version: '1',
    }

    await expect(() => rpc.call(callOptions)).rejects.toEqual(expect.any(Error))
  })

  it('handles duplicate in-flight calls', async () => {
    const callOptions = {
      args: { delayCallForTesting: 25 },
      procedure: 'duplicate-call',
      scope: 'inflight',
      version: '1',
    }

    rpc.call(callOptions)
    rpc.call(callOptions)

    return new Promise((done) => {
      // we have to wait because a Call is async and we can't get the in-flight count immediately
      setTimeout(() => {
        expect(rpc.getInFlightCallCount()).toEqual(1)

        done(true)
      }, 10)
    })
  })

  it('intercepts requests', async () => {
    const rpc = new RPCClient({
      cacheMaxAgeMs: 100,
      deadlineMs: 100,
      requestInterceptor: (request: CallRequestDTO) => {
        request.args.count = request.args.count + 1

        return request
      },
      transports: {
        http: new MockHTTPTransport(),
      },
    })

    const callOptions = {
      args: { count: 1 },
      procedure: 'intercept-request',
      scope: 'intercept',
      version: '1',
    }

    const { data } = await rpc.call(callOptions)

    expect(data.count).toEqual(2)
  })

  it('intercepts responses', async () => {
    const rpc = new RPCClient({
      cacheMaxAgeMs: 100,
      deadlineMs: 100,
      responseInterceptor: (
        response: CallResponseDTO,
        request: CallRequestDTO,
      ) => {
        response.data.count = response.data.count / 2 + request.args.count

        return response
      },
      transports: {
        http: new MockHTTPTransport(),
      },
    })

    const callOptions = {
      args: { count: 10 },
      procedure: 'intercept-response',
      scope: 'intercept',
      version: '1',
    }

    const { data } = await rpc.call(callOptions)

    expect(data.count).toEqual(15)
  })
})
