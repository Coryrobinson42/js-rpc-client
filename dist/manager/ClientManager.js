import { __awaiter } from '../node_modules/tslib/tslib.es6.js';
import { CallRequestError } from '../errors/CallRequestError.js';
import { CallRequestTransportError } from '../transports/errors/CallRequestTransportError.js';
import { ManagerCallRequestTimeoutError } from './errors/ManagerCallRequestTimeoutError.js';
import { makeCallRequestKey } from '../cache/utils/cache-utils.js';
import { cloneDeep } from 'lodash-es';
import { GetDebugLogger } from '../utils/debug-logger.js';

const debug = GetDebugLogger('rpc:ClientManager');
class ClientManager {
    constructor(options) {
        this.options = options;
        this.inflightCallsByKey = {};
        this.interceptors = {
            request: [],
            response: [],
        };
        this.addResponseInterceptor = (interceptor) => {
            if (typeof interceptor !== 'function')
                throw new Error('cannot add interceptor that is not a function');
            this.interceptors.response.push(interceptor);
        };
        this.addRequestInterceptor = (interceptor) => {
            if (typeof interceptor !== 'function')
                throw new Error('cannot add interceptor that is not a function');
            this.interceptors.request.push(interceptor);
        };
        this.clearCache = (request) => {
            debug('clearCache');
            if (!this.cache)
                return;
            if (request) {
                this.cache.setCachedResponse(request, undefined);
            }
            else {
                this.cache.clearCache();
            }
        };
        this.getCachedResponse = (request) => {
            var _a;
            debug('getCachedResponse', request);
            return (_a = this === null || this === void 0 ? void 0 : this.cache) === null || _a === void 0 ? void 0 : _a.getCachedResponse(request);
        };
        this.getInFlightCallCount = () => {
            debug('getInFlightCallCount');
            return Object.keys(this.inflightCallsByKey).length;
        };
        this.manageClientRequest = (originalRequest) => __awaiter(this, void 0, void 0, function* () {
            debug('manageClientRequest', originalRequest);
            const request = yield this.interceptRequestMutator(originalRequest);
            const key = makeCallRequestKey(request);
            if (this.inflightCallsByKey.hasOwnProperty(key)) {
                debug('manageClientRequest using an existing in-flight call', key);
                return this.inflightCallsByKey[key].then((res) => {
                    const response = cloneDeep(res);
                    if (originalRequest.correlationId) {
                        response.correlationId = originalRequest.correlationId;
                    }
                    return response;
                });
            }
            const requestPromiseWrapper = () => __awaiter(this, void 0, void 0, function* () {
                const transportResponse = yield this.sendRequestWithTransport(request);
                const response = yield this.interceptResponseMutator(transportResponse, request);
                if (this.cache && response) {
                    this.cache.setCachedResponse(request, response);
                }
                return response;
            });
            const requestPromise = requestPromiseWrapper()
                .then((response) => {
                delete this.inflightCallsByKey[key];
                return response;
            })
                .catch((err) => {
                delete this.inflightCallsByKey[key];
                throw err;
            });
            this.inflightCallsByKey[key] = requestPromise;
            return yield requestPromise;
        });
        this.setIdentity = (identity) => {
            debug('setIdentity', identity);
            this.transports.forEach((transport) => transport.setIdentity(identity));
        };
        this.interceptRequestMutator = (originalRequest) => __awaiter(this, void 0, void 0, function* () {
            let request = originalRequest;
            if (this.interceptors.request.length) {
                debug('interceptRequestMutator(), original request:', originalRequest);
                for (const interceptor of this.interceptors.request) {
                    request = yield interceptor(request);
                }
            }
            return request;
        });
        this.interceptResponseMutator = (originalResponse, request) => __awaiter(this, void 0, void 0, function* () {
            let response = originalResponse;
            if (this.interceptors.response.length) {
                debug('interceptResponseMutator', request, originalResponse);
                for (const interceptor of this.interceptors.response) {
                    try {
                        response = yield interceptor(response, request);
                    }
                    catch (e) {
                        debug('caught response interceptor, request:', request, 'original response:', originalResponse, 'mutated response:', response);
                        throw e;
                    }
                }
            }
            return response;
        });
        this.sendRequestWithTransport = (request) => __awaiter(this, void 0, void 0, function* () {
            debug('sendRequestWithTransport', request);
            let response;
            let transportsIndex = 0;
            while (!response && this.transports[transportsIndex]) {
                const transport = this.transports[transportsIndex];
                if (!transport.isConnected()) {
                    transportsIndex++;
                    continue;
                }
                debug(`sendRequestWithTransport trying ${this.transports[transportsIndex].name}`, request);
                try {
                    let timeout;
                    const deadlinePromise = new Promise((_, reject) => {
                        timeout = setTimeout(() => {
                            reject(new ManagerCallRequestTimeoutError(`Procedure ${request.procedure}`));
                        }, this.options.deadlineMs);
                    });
                    response = yield Promise.race([
                        deadlinePromise,
                        transport.sendRequest(request),
                    ]).then((response) => {
                        clearTimeout(timeout);
                        return response;
                    });
                }
                catch (e) {
                    if (e instanceof ManagerCallRequestTimeoutError ||
                        e instanceof CallRequestTransportError) {
                        debug(`sending request with ${this.transports[transportsIndex].name} failed, trying next transport`);
                        transportsIndex++;
                    }
                    else {
                        throw e;
                    }
                }
            }
            if (!response && !this.transports[transportsIndex]) {
                throw new CallRequestError(`Procedure ${request.procedure} did not get a response from any transports`);
            }
            else if (!response) {
                throw new CallRequestError(`Procedure ${request.procedure} did not get a response from the remote`);
            }
            return response;
        });
        this.cache = options.cache;
        if (options.requestInterceptor) {
            this.interceptors.request.push(options.requestInterceptor);
        }
        if (options.responseInterceptor) {
            this.interceptors.response.push(options.responseInterceptor);
        }
        this.transports = options.transports;
    }
}

export { ClientManager };
//# sourceMappingURL=ClientManager.js.map
