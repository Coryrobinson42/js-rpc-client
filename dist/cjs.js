'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var stringify = require('fast-json-stable-stringify');
var lodashEs = require('lodash-es');
var LRU = require('lru-cache');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var stringify__default = /*#__PURE__*/_interopDefaultLegacy(stringify);
var LRU__default = /*#__PURE__*/_interopDefaultLegacy(LRU);

/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

const DEFAULT_REQUEST_SCOPE = 'global';
const DEFAULT_REQUEST_VERSION = '1';
class CallRequestDTO {
    constructor(call) {
        const { args, correlationId, identity, procedure, scope, version } = call;
        this.args = args;
        if (correlationId && typeof correlationId !== 'string')
            throw new Error('correlationId must be a string');
        this.correlationId = correlationId || Math.random().toString();
        if (identity) {
            if (typeof identity !== 'object')
                throw new Error('identity must be an object');
            if (identity.authorization && typeof identity.authorization !== 'string')
                throw new Error('identity.authorization must be a string');
            if (identity.deviceName && typeof identity.deviceName !== 'string')
                throw new Error('identity.deviceName must be a string');
            if (identity.metadata && typeof identity.metadata !== 'object')
                throw new Error('identity.metadata must be a object');
            this.identity = identity;
        }
        if (procedure && typeof procedure !== 'string')
            throw new Error('procedure must be string');
        this.procedure = procedure;
        if (scope && typeof scope !== 'string')
            throw new Error('scope must be string');
        this.scope = scope;
        if (version && typeof version !== 'string')
            throw new Error('version must be string');
        this.version = version;
    }
}

class CallRequestError extends Error {
}

class CallRequestTransportError extends Error {
}

class ManagerCallRequestTimeoutError extends Error {
}

function isPlainObject(value) {
    if (Object.prototype.toString.call(value) !== '[object Object]') {
        return false;
    }
    const prototype = Object.getPrototypeOf(value);
    return prototype === null || prototype === Object.prototype;
}
function sortKeys(object, options = {}) {
    if (!isPlainObject(object) && !Array.isArray(object)) {
        throw new TypeError('Expected a plain object or array');
    }
    const { deep, compare } = options;
    const seenInput = [];
    const seenOutput = [];
    const deepSortArray = (array) => {
        const seenIndex = seenInput.indexOf(array);
        if (seenIndex !== -1) {
            return seenOutput[seenIndex];
        }
        const result = [];
        seenInput.push(array);
        seenOutput.push(result);
        result.push(...array.map((item) => {
            if (Array.isArray(item)) {
                return deepSortArray(item);
            }
            if (isPlainObject(item)) {
                return _sortKeys(item);
            }
            return item;
        }));
        return result;
    };
    const _sortKeys = (object) => {
        const seenIndex = seenInput.indexOf(object);
        if (seenIndex !== -1) {
            return seenOutput[seenIndex];
        }
        const result = {};
        const keys = Object.keys(object).sort(compare);
        seenInput.push(object);
        seenOutput.push(result);
        for (const key of keys) {
            const value = object[key];
            let newValue;
            if (deep && Array.isArray(value)) {
                newValue = deepSortArray(value);
            }
            else {
                newValue = deep && isPlainObject(value) ? _sortKeys(value) : value;
            }
            Object.defineProperty(result, key, Object.assign(Object.assign({}, Object.getOwnPropertyDescriptor(object, key)), { value: newValue }));
        }
        return result;
    };
    if (Array.isArray(object)) {
        return deep ? deepSortArray(object) : object.slice();
    }
    return _sortKeys(object);
}

const makeCallRequestKey = (request) => {
    const strippedCallRequest = {
        args: request.args,
        procedure: request.procedure,
        scope: request.scope,
        version: request.version,
    };
    let key;
    if (Array.isArray(strippedCallRequest) ||
        typeof strippedCallRequest === 'object') {
        key = stringify__default["default"](sortKeys(strippedCallRequest, { deep: true }));
    }
    else {
        console.warn('makeCallRequestKey() was not passed a CallRequestDTO', request);
        throw new Error('makeCallRequestKey() was not passed a CallRequestDTO');
    }
    return key;
};

let lsDebug = null;
try {
    lsDebug = localStorage.getItem('debug') || '';
}
catch (e) {
    if (typeof window !== 'undefined' && typeof localStorage !== 'undefined') {
        console.warn('Error checking window.debug');
    }
}
const GetDebugLogger = (prefix) => {
    if (lsDebug) {
        if (new RegExp(lsDebug).test(prefix)) {
            return (...args) => console.log(prefix, ...args);
        }
    }
    return (() => undefined);
};

const debug$3 = GetDebugLogger('rpc:ClientManager');
class ClientManager {
    constructor(options) {
        this.options = options;
        this.inflightCallsByKey = {};
        this.interceptors = {
            request: [],
            response: [],
        };
        this.addResponseInterceptor = (interceptor) => {
            if (typeof interceptor !== 'function')
                throw new Error('cannot add interceptor that is not a function');
            this.interceptors.response.push(interceptor);
        };
        this.addRequestInterceptor = (interceptor) => {
            if (typeof interceptor !== 'function')
                throw new Error('cannot add interceptor that is not a function');
            this.interceptors.request.push(interceptor);
        };
        this.clearCache = (request) => {
            debug$3('clearCache');
            if (!this.cache)
                return;
            if (request) {
                this.cache.setCachedResponse(request, undefined);
            }
            else {
                this.cache.clearCache();
            }
        };
        this.getCachedResponse = (request) => {
            var _a;
            debug$3('getCachedResponse', request);
            return (_a = this === null || this === void 0 ? void 0 : this.cache) === null || _a === void 0 ? void 0 : _a.getCachedResponse(request);
        };
        this.getInFlightCallCount = () => {
            debug$3('getInFlightCallCount');
            return Object.keys(this.inflightCallsByKey).length;
        };
        this.manageClientRequest = (originalRequest) => __awaiter(this, void 0, void 0, function* () {
            debug$3('manageClientRequest', originalRequest);
            const request = yield this.interceptRequestMutator(originalRequest);
            const key = makeCallRequestKey(request);
            if (this.inflightCallsByKey.hasOwnProperty(key)) {
                debug$3('manageClientRequest using an existing in-flight call', key);
                return this.inflightCallsByKey[key].then((res) => {
                    const response = lodashEs.cloneDeep(res);
                    if (originalRequest.correlationId) {
                        response.correlationId = originalRequest.correlationId;
                    }
                    return response;
                });
            }
            const requestPromiseWrapper = () => __awaiter(this, void 0, void 0, function* () {
                const transportResponse = yield this.sendRequestWithTransport(request);
                const response = yield this.interceptResponseMutator(transportResponse, request);
                if (this.cache && response) {
                    this.cache.setCachedResponse(request, response);
                }
                return response;
            });
            const requestPromise = requestPromiseWrapper()
                .then((response) => {
                delete this.inflightCallsByKey[key];
                return response;
            })
                .catch((err) => {
                delete this.inflightCallsByKey[key];
                throw err;
            });
            this.inflightCallsByKey[key] = requestPromise;
            return yield requestPromise;
        });
        this.setIdentity = (identity) => {
            debug$3('setIdentity', identity);
            this.transports.forEach((transport) => transport.setIdentity(identity));
        };
        this.interceptRequestMutator = (originalRequest) => __awaiter(this, void 0, void 0, function* () {
            let request = originalRequest;
            if (this.interceptors.request.length) {
                debug$3('interceptRequestMutator(), original request:', originalRequest);
                for (const interceptor of this.interceptors.request) {
                    request = yield interceptor(request);
                }
            }
            return request;
        });
        this.interceptResponseMutator = (originalResponse, request) => __awaiter(this, void 0, void 0, function* () {
            let response = originalResponse;
            if (this.interceptors.response.length) {
                debug$3('interceptResponseMutator', request, originalResponse);
                for (const interceptor of this.interceptors.response) {
                    try {
                        response = yield interceptor(response, request);
                    }
                    catch (e) {
                        debug$3('caught response interceptor, request:', request, 'original response:', originalResponse, 'mutated response:', response);
                        throw e;
                    }
                }
            }
            return response;
        });
        this.sendRequestWithTransport = (request) => __awaiter(this, void 0, void 0, function* () {
            debug$3('sendRequestWithTransport', request);
            let response;
            let transportsIndex = 0;
            while (!response && this.transports[transportsIndex]) {
                const transport = this.transports[transportsIndex];
                if (!transport.isConnected()) {
                    transportsIndex++;
                    continue;
                }
                debug$3(`sendRequestWithTransport trying ${this.transports[transportsIndex].name}`, request);
                try {
                    let timeout;
                    const deadlinePromise = new Promise((_, reject) => {
                        timeout = setTimeout(() => {
                            reject(new ManagerCallRequestTimeoutError(`Procedure ${request.procedure}`));
                        }, this.options.deadlineMs);
                    });
                    response = yield Promise.race([
                        deadlinePromise,
                        transport.sendRequest(request),
                    ]).then((response) => {
                        clearTimeout(timeout);
                        return response;
                    });
                }
                catch (e) {
                    if (e instanceof ManagerCallRequestTimeoutError ||
                        e instanceof CallRequestTransportError) {
                        debug$3(`sending request with ${this.transports[transportsIndex].name} failed, trying next transport`);
                        transportsIndex++;
                    }
                    else {
                        throw e;
                    }
                }
            }
            if (!response && !this.transports[transportsIndex]) {
                throw new CallRequestError(`Procedure ${request.procedure} did not get a response from any transports`);
            }
            else if (!response) {
                throw new CallRequestError(`Procedure ${request.procedure} did not get a response from the remote`);
            }
            return response;
        });
        this.cache = options.cache;
        if (options.requestInterceptor) {
            this.interceptors.request.push(options.requestInterceptor);
        }
        if (options.responseInterceptor) {
            this.interceptors.response.push(options.responseInterceptor);
        }
        this.transports = options.transports;
    }
}

class BrowserCache {
    constructor(cacheOptions) {
        this.cacheOptions = cacheOptions;
    }
    clearCache() {
    }
    getCachedResponse(request) {
        return undefined;
    }
    setCachedResponse(request, response) {
    }
}
BrowserCache.DEFAULT_CACHE_MAX_AGE_MS = 1000 * 60 * 5;
BrowserCache.DEFAULT_CACHE_MAX_SIZE = 100;

const debug$2 = GetDebugLogger('rpc:InMemoryCache');
class InMemoryCache {
    constructor(cacheOptions) {
        this.clearCache = () => {
            var _a;
            debug$2('clearCache');
            (_a = this.cachedResponseByParams) === null || _a === void 0 ? void 0 : _a.reset();
        };
        this.getCachedResponse = (request) => {
            var _a;
            debug$2('getCachedResponse, key: ', request);
            let cachedResponse = (_a = this.cachedResponseByParams) === null || _a === void 0 ? void 0 : _a.get(makeCallRequestKey(request));
            if (typeof cachedResponse === 'string') {
                cachedResponse = JSON.parse(cachedResponse);
            }
            return cachedResponse;
        };
        this.setCachedResponse = (request, response) => {
            var _a, _b;
            debug$2('setCachedResponse', request, response);
            const requestKey = makeCallRequestKey(request);
            if (!response) {
                return (_a = this.cachedResponseByParams) === null || _a === void 0 ? void 0 : _a.del(requestKey);
            }
            const cachedResponse = Object.assign({}, response);
            delete cachedResponse.correlationId;
            (_b = this.cachedResponseByParams) === null || _b === void 0 ? void 0 : _b.set(requestKey, cachedResponse ? JSON.stringify(cachedResponse) : undefined);
        };
        this.cachedResponseByParams = new LRU__default["default"]({
            max: (cacheOptions === null || cacheOptions === void 0 ? void 0 : cacheOptions.cacheMaxSize) || InMemoryCache.DEFAULT_CACHE_MAX_SIZE,
            maxAge: (cacheOptions === null || cacheOptions === void 0 ? void 0 : cacheOptions.cacheMaxAgeMs) || InMemoryCache.DEFAULT_CACHE_MAX_AGE_MS,
        });
    }
}
InMemoryCache.DEFAULT_CACHE_MAX_AGE_MS = 1000 * 60 * 5;
InMemoryCache.DEFAULT_CACHE_MAX_SIZE = 100;

class CallResponseDTO {
    constructor(response) {
        const { code, correlationId, data, message, success } = response;
        if (typeof code !== 'number')
            throw new Error('code must be a number');
        this.code = code;
        if (correlationId && typeof correlationId !== 'string')
            throw new Error('correlationId must be a string');
        this.correlationId = correlationId;
        this.data = data;
        if (message && typeof message !== 'string')
            throw new Error('message must be a string');
        this.message = message;
        if (typeof success !== 'boolean')
            throw new Error('success must be a boolean');
        this.success = success;
    }
}

const debug$1 = GetDebugLogger('rpc:HTTPTransport');
class HTTPTransport {
    constructor(options) {
        this.options = options;
        this.networkIsConnected = false;
        this.name = 'HttpTransport';
        this.isConnected = () => {
            var _a;
            if (typeof window !== 'undefined') {
                return !!((_a = window === null || window === void 0 ? void 0 : window.navigator) === null || _a === void 0 ? void 0 : _a.onLine);
            }
            return this.networkIsConnected;
        };
        this.sendRequest = (call) => __awaiter(this, void 0, void 0, function* () {
            debug$1('sendRequest', call);
            const body = JSON.stringify(Object.assign({ identity: this.identity }, call));
            const res = yield (window === null || window === void 0 ? void 0 : window.fetch(this.host, {
                body,
                cache: 'default',
                credentials: 'omit',
                headers: {
                    'Content-Type': 'application/json',
                },
                method: 'POST',
                mode: 'cors',
                redirect: 'follow',
                referrerPolicy: 'origin',
            }));
            const string = yield res.text();
            if (!string)
                throw new Error('No response received from remote');
            const response = JSON.parse(string);
            return new CallResponseDTO(response);
        });
        this.setIdentity = (identity) => {
            debug$1('setIdentity', identity);
            this.identity = identity;
        };
        this.host = options.host;
    }
}

const DEFAULT_TIMEOUT = 30000;
class PromiseWrapper {
    constructor(timeout) {
        this.reject = (rejectReturnValue) => {
            if (this.rejectPromise) {
                this.rejectPromise(rejectReturnValue);
            }
        };
        this.resolve = (resolveReturnValue) => {
            if (this.resolvePromise) {
                this.resolvePromise(resolveReturnValue);
            }
        };
        this.promise = new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            const timer = setTimeout(() => {
                reject(new Error('PromiseWraper timeout'));
            }, timeout || DEFAULT_TIMEOUT);
            this.rejectPromise = (arg) => {
                clearTimeout(timer);
                reject(arg);
            };
            this.resolvePromise = (arg) => {
                clearTimeout(timer);
                resolve(arg);
            };
        }));
    }
}

const sleep = (ms) => new Promise((r) => setTimeout(r, ms));

const debug = GetDebugLogger('rpc:WebSocketTransport');
class WebSocketTransport {
    constructor(options) {
        this.options = options;
        this.connectedToRemote = false;
        this.isWaitingForIdentityConfirmation = false;
        this.pendingPromisesForResponse = {};
        this.websocketId = undefined;
        this.name = 'WebSocketTransport';
        this.isConnected = () => {
            return this.connectedToRemote && !!this.websocket;
        };
        this.sendRequest = (call) => __awaiter(this, void 0, void 0, function* () {
            debug('sendRequest', call);
            while (this.isWaitingForIdentityConfirmation) {
                debug('waiting for identity confirmation');
                yield sleep(100);
            }
            return this.send(call);
        });
        this.setIdentity = (identity) => {
            debug('setIdentity', identity);
            if (!identity) {
                this.identity = undefined;
                this.resetConnection();
                return;
            }
            else {
                this.identity = identity;
            }
            if (!this.connectedToRemote) {
                debug('setIdentity is not connected to remote');
                return;
            }
            this.isWaitingForIdentityConfirmation = true;
            this.send({ identity })
                .then(() => {
                debug('setIdentity with remote complete');
            })
                .catch((e) => {
                debug('setIdentity with remote error', e);
            })
                .finally(() => {
                this.isWaitingForIdentityConfirmation = false;
            });
        };
        this.connect = () => {
            debug('connect', this.host);
            if (this.websocket) {
                debug('connect() returning early, websocket already exists');
                return;
            }
            this.websocketId = Math.random();
            this.websocket = new WebSocket(this.host);
            const ws = this.websocket;
            ws.onopen = () => {
                console.log('WebSocket connected:');
                this.setConnectedToRemote(true);
                if (this.identity) {
                    this.setIdentity(this.identity);
                }
            };
            ws.onmessage = (msg) => {
                this.handleWebSocketMsg(msg);
            };
            ws.onclose = (e) => {
                if (this.connectedToRemote) {
                    console.log('WebSocket closed', e.reason);
                }
                else {
                    debug('WebSocket closed, it was not connected to the remote');
                }
                this.setConnectedToRemote(false);
                this.isWaitingForIdentityConfirmation = false;
                this.websocket = undefined;
                this.websocketId = undefined;
                Object.entries(this.pendingPromisesForResponse).forEach(([correlationId, promiseWrapper]) => {
                    promiseWrapper.reject(new CallRequestTransportError('Websocket disconnected'));
                });
                if (!e.wasClean) {
                    setTimeout(() => {
                        this.connect();
                    }, 1000);
                }
            };
            ws.onerror = (err) => {
                if (this.connectedToRemote) {
                    console.error('Socket encountered error: ', err);
                }
                else {
                    debug('WebSocket errored, it was not connected to the remote');
                }
                ws.close();
            };
        };
        this.disconnect = () => {
            var _a;
            debug('disconnect');
            this.setConnectedToRemote(false);
            this.isWaitingForIdentityConfirmation = false;
            (_a = this.websocket) === null || _a === void 0 ? void 0 : _a.close();
            this.websocket = undefined;
            this.websocketId = undefined;
        };
        this.handleWebSocketMsg = (msg) => {
            debug('handleWebSocketMsg', msg);
            let json;
            try {
                json = JSON.parse(msg.data);
            }
            catch (e) {
                console.error('error parsing WS msg', e);
            }
            const callResponseDTO = new CallResponseDTO(json);
            if (!callResponseDTO.correlationId) {
                console.error('RPCClient WebSocketTransport received unexpected msg from the server, not correlationId found in response.', json);
                return;
            }
            const pendingRequestPromise = this.pendingPromisesForResponse[callResponseDTO.correlationId];
            if (pendingRequestPromise) {
                pendingRequestPromise.resolve(callResponseDTO);
            }
            else {
                console.warn("rcvd WS msg/response that doesn't match any pending RPC's", json);
            }
        };
        this.resetConnection = () => {
            debug('resetConnection');
            this.disconnect();
            this.connect();
        };
        this.send = (call) => __awaiter(this, void 0, void 0, function* () {
            var _a;
            debug('send', call);
            const correlationId = Math.random().toString();
            call.correlationId = call.correlationId || correlationId;
            let promiseWrapper = new PromiseWrapper(WebSocketTransport.DEFAULT_TIMEOUT_MS);
            (_a = this.websocket) === null || _a === void 0 ? void 0 : _a.send(JSON.stringify(call));
            this.pendingPromisesForResponse[call.correlationId] = promiseWrapper;
            return yield promiseWrapper.promise.finally(() => {
                delete this.pendingPromisesForResponse[call.correlationId];
            });
        });
        this.setConnectedToRemote = (connected) => {
            this.connectedToRemote = connected;
            if (this.options.onConnectionStatusChange) {
                this.options.onConnectionStatusChange(connected);
            }
        };
        debug('new WebSocketTransport()');
        this.host = options.host;
        this.connect();
    }
}
WebSocketTransport.DEFAULT_TIMEOUT_MS = 10000;

const parseRequestShorthand = (requestString) => {
    const parsed = requestString.split('::');
    return {
        procedure: parsed[1],
        scope: parsed[0] || DEFAULT_REQUEST_SCOPE,
        version: parsed[2] || DEFAULT_REQUEST_VERSION,
    };
};

const ERRORS = {
    HTTP_HOST_OR_TRANSPORT_REQUIRED: `http host or tansport is required`,
    INTERCEPTOR_MUSTBE_FUNC: `interceptors must be a function`,
};
class RPCClient {
    constructor(options) {
        var _a, _b, _c, _d, _e, _f;
        this.options = options;
        this.call = (request, args) => __awaiter(this, void 0, void 0, function* () {
            if (!request)
                throw new Error('RPCClient.call(request) requires a "request" param');
            let req;
            if (typeof request === 'string')
                req = parseRequestShorthand(request);
            else
                req = request;
            if (args) {
                req.args = args;
            }
            const requestDTO = new CallRequestDTO(req);
            if (!requestDTO.procedure && !requestDTO.identity) {
                throw new TypeError('RPCClient#call requires a "identity" or "procedure" prop and received neither');
            }
            if (!requestDTO.identity)
                requestDTO.identity = {};
            requestDTO.identity = lodashEs.merge(Object.assign({}, this.identity), requestDTO.identity);
            const callResponse = yield this.callManager.manageClientRequest(requestDTO);
            if (!callResponse.success) {
                throw callResponse;
            }
            return callResponse;
        });
        this.clearCache = (request) => {
            this.callManager.clearCache(request);
        };
        this.getCallCache = (request, args) => {
            let req;
            if (typeof request === 'string')
                req = parseRequestShorthand(request);
            else
                req = request;
            if (args) {
                req.args = args;
            }
            const cachedResponse = this.callManager.getCachedResponse(req);
            if (cachedResponse)
                return cachedResponse;
            return undefined;
        };
        this.getIdentity = () => this.identity ? lodashEs.cloneDeep(this.identity) : this.identity;
        this.getInFlightCallCount = () => {
            return this.callManager.getInFlightCallCount();
        };
        this.getWebSocketConnected = () => {
            var _a;
            return !!((_a = this === null || this === void 0 ? void 0 : this.webSocketTransport) === null || _a === void 0 ? void 0 : _a.isConnected());
        };
        this.makeProcedure = (request) => {
            const self = this;
            let req;
            if (typeof request === 'string')
                req = parseRequestShorthand(request);
            else
                req = request;
            return function curriedProcedure(args) {
                return self.call(Object.assign(Object.assign({}, req), { args }));
            };
        };
        this.registerResponseInterceptor = (responseInterceptor) => {
            this.callManager.addResponseInterceptor(responseInterceptor);
        };
        this.registerRequestInterceptor = (requestInterceptor) => {
            this.callManager.addRequestInterceptor(requestInterceptor);
        };
        this.setIdentity = (identity) => {
            this.identity = identity;
            this.callManager.setIdentity(identity);
        };
        if (!((_a = options === null || options === void 0 ? void 0 : options.hosts) === null || _a === void 0 ? void 0 : _a.http) && !((_b = options === null || options === void 0 ? void 0 : options.transports) === null || _b === void 0 ? void 0 : _b.http)) {
            throw new Error(ERRORS.HTTP_HOST_OR_TRANSPORT_REQUIRED);
        }
        if (options.requestInterceptor &&
            typeof options.requestInterceptor !== 'function') {
            throw new Error(ERRORS.INTERCEPTOR_MUSTBE_FUNC);
        }
        if (options.responseInterceptor &&
            typeof options.responseInterceptor !== 'function') {
            throw new Error(ERRORS.INTERCEPTOR_MUSTBE_FUNC);
        }
        let cache;
        const cacheOptions = { cacheMaxAgeMs: options === null || options === void 0 ? void 0 : options.cacheMaxAgeMs };
        if (options.cacheType == 'browser') {
            cache = new BrowserCache(cacheOptions);
        }
        else {
            cache = new InMemoryCache(cacheOptions);
        }
        const transports = [];
        if ((_c = options.transports) === null || _c === void 0 ? void 0 : _c.websocket) {
            transports.push(options.transports.websocket);
        }
        else if ((_d = options === null || options === void 0 ? void 0 : options.hosts) === null || _d === void 0 ? void 0 : _d.websocket) {
            this.webSocketTransport = new WebSocketTransport({
                host: options.hosts.websocket,
                onConnectionStatusChange: options.onWebSocketConnectionStatusChange,
            });
            transports.push(this.webSocketTransport);
        }
        if ((_e = options.transports) === null || _e === void 0 ? void 0 : _e.http) {
            transports.push(options.transports.http);
        }
        else if ((_f = options === null || options === void 0 ? void 0 : options.hosts) === null || _f === void 0 ? void 0 : _f.http) {
            this.httpTransport = new HTTPTransport({ host: options.hosts.http });
            transports.push(this.httpTransport);
        }
        this.callManager = new ClientManager({
            cache,
            deadlineMs: options.deadlineMs || RPCClient.DEFAULT_DEADLINE_MS,
            requestInterceptor: options.requestInterceptor,
            responseInterceptor: options.responseInterceptor,
            transports,
        });
    }
}
RPCClient.DEFAULT_DEADLINE_MS = 10000;

exports.RPCClient = RPCClient;
exports.RPCResponse = CallResponseDTO;
//# sourceMappingURL=cjs.js.map
