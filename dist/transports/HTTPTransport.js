import { __awaiter } from '../node_modules/tslib/tslib.es6.js';
import { CallResponseDTO } from '../CallResponseDTO.js';
import { GetDebugLogger } from '../utils/debug-logger.js';

const debug = GetDebugLogger('rpc:HTTPTransport');
class HTTPTransport {
    constructor(options) {
        this.options = options;
        this.networkIsConnected = false;
        this.name = 'HttpTransport';
        this.isConnected = () => {
            var _a;
            if (typeof window !== 'undefined') {
                return !!((_a = window === null || window === void 0 ? void 0 : window.navigator) === null || _a === void 0 ? void 0 : _a.onLine);
            }
            return this.networkIsConnected;
        };
        this.sendRequest = (call) => __awaiter(this, void 0, void 0, function* () {
            debug('sendRequest', call);
            const body = JSON.stringify(Object.assign({ identity: this.identity }, call));
            const res = yield (window === null || window === void 0 ? void 0 : window.fetch(this.host, {
                body,
                cache: 'default',
                credentials: 'omit',
                headers: {
                    'Content-Type': 'application/json',
                },
                method: 'POST',
                mode: 'cors',
                redirect: 'follow',
                referrerPolicy: 'origin',
            }));
            const string = yield res.text();
            if (!string)
                throw new Error('No response received from remote');
            const response = JSON.parse(string);
            return new CallResponseDTO(response);
        });
        this.setIdentity = (identity) => {
            debug('setIdentity', identity);
            this.identity = identity;
        };
        this.host = options.host;
    }
}

export { HTTPTransport };
//# sourceMappingURL=HTTPTransport.js.map
