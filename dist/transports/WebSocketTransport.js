import { __awaiter } from '../node_modules/tslib/tslib.es6.js';
import { CallResponseDTO } from '../CallResponseDTO.js';
import { PromiseWrapper } from '../utils/PromiseWrapper.js';
import { sleep } from '../utils/sleep.js';
import { GetDebugLogger } from '../utils/debug-logger.js';
import { CallRequestTransportError } from './errors/CallRequestTransportError.js';

const debug = GetDebugLogger('rpc:WebSocketTransport');
class WebSocketTransport {
    constructor(options) {
        this.options = options;
        this.connectedToRemote = false;
        this.isWaitingForIdentityConfirmation = false;
        this.pendingPromisesForResponse = {};
        this.websocketId = undefined;
        this.name = 'WebSocketTransport';
        this.isConnected = () => {
            return this.connectedToRemote && !!this.websocket;
        };
        this.sendRequest = (call) => __awaiter(this, void 0, void 0, function* () {
            debug('sendRequest', call);
            while (this.isWaitingForIdentityConfirmation) {
                debug('waiting for identity confirmation');
                yield sleep(100);
            }
            return this.send(call);
        });
        this.setIdentity = (identity) => {
            debug('setIdentity', identity);
            if (!identity) {
                this.identity = undefined;
                this.resetConnection();
                return;
            }
            else {
                this.identity = identity;
            }
            if (!this.connectedToRemote) {
                debug('setIdentity is not connected to remote');
                return;
            }
            this.isWaitingForIdentityConfirmation = true;
            this.send({ identity })
                .then(() => {
                debug('setIdentity with remote complete');
            })
                .catch((e) => {
                debug('setIdentity with remote error', e);
            })
                .finally(() => {
                this.isWaitingForIdentityConfirmation = false;
            });
        };
        this.connect = () => {
            debug('connect', this.host);
            if (this.websocket) {
                debug('connect() returning early, websocket already exists');
                return;
            }
            this.websocketId = Math.random();
            this.websocket = new WebSocket(this.host);
            const ws = this.websocket;
            ws.onopen = () => {
                console.log('WebSocket connected:');
                this.setConnectedToRemote(true);
                if (this.identity) {
                    this.setIdentity(this.identity);
                }
            };
            ws.onmessage = (msg) => {
                this.handleWebSocketMsg(msg);
            };
            ws.onclose = (e) => {
                if (this.connectedToRemote) {
                    console.log('WebSocket closed', e.reason);
                }
                else {
                    debug('WebSocket closed, it was not connected to the remote');
                }
                this.setConnectedToRemote(false);
                this.isWaitingForIdentityConfirmation = false;
                this.websocket = undefined;
                this.websocketId = undefined;
                Object.entries(this.pendingPromisesForResponse).forEach(([correlationId, promiseWrapper]) => {
                    promiseWrapper.reject(new CallRequestTransportError('Websocket disconnected'));
                });
                if (!e.wasClean) {
                    setTimeout(() => {
                        this.connect();
                    }, 1000);
                }
            };
            ws.onerror = (err) => {
                if (this.connectedToRemote) {
                    console.error('Socket encountered error: ', err);
                }
                else {
                    debug('WebSocket errored, it was not connected to the remote');
                }
                ws.close();
            };
        };
        this.disconnect = () => {
            var _a;
            debug('disconnect');
            this.setConnectedToRemote(false);
            this.isWaitingForIdentityConfirmation = false;
            (_a = this.websocket) === null || _a === void 0 ? void 0 : _a.close();
            this.websocket = undefined;
            this.websocketId = undefined;
        };
        this.handleWebSocketMsg = (msg) => {
            debug('handleWebSocketMsg', msg);
            let json;
            try {
                json = JSON.parse(msg.data);
            }
            catch (e) {
                console.error('error parsing WS msg', e);
            }
            const callResponseDTO = new CallResponseDTO(json);
            if (!callResponseDTO.correlationId) {
                console.error('RPCClient WebSocketTransport received unexpected msg from the server, not correlationId found in response.', json);
                return;
            }
            const pendingRequestPromise = this.pendingPromisesForResponse[callResponseDTO.correlationId];
            if (pendingRequestPromise) {
                pendingRequestPromise.resolve(callResponseDTO);
            }
            else {
                console.warn("rcvd WS msg/response that doesn't match any pending RPC's", json);
            }
        };
        this.resetConnection = () => {
            debug('resetConnection');
            this.disconnect();
            this.connect();
        };
        this.send = (call) => __awaiter(this, void 0, void 0, function* () {
            var _a;
            debug('send', call);
            const correlationId = Math.random().toString();
            call.correlationId = call.correlationId || correlationId;
            let promiseWrapper = new PromiseWrapper(WebSocketTransport.DEFAULT_TIMEOUT_MS);
            (_a = this.websocket) === null || _a === void 0 ? void 0 : _a.send(JSON.stringify(call));
            this.pendingPromisesForResponse[call.correlationId] = promiseWrapper;
            return yield promiseWrapper.promise.finally(() => {
                delete this.pendingPromisesForResponse[call.correlationId];
            });
        });
        this.setConnectedToRemote = (connected) => {
            this.connectedToRemote = connected;
            if (this.options.onConnectionStatusChange) {
                this.options.onConnectionStatusChange(connected);
            }
        };
        debug('new WebSocketTransport()');
        this.host = options.host;
        this.connect();
    }
}
WebSocketTransport.DEFAULT_TIMEOUT_MS = 10000;

export { WebSocketTransport };
//# sourceMappingURL=WebSocketTransport.js.map
