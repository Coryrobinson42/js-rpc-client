import { DEFAULT_REQUEST_SCOPE, DEFAULT_REQUEST_VERSION } from '../CallRequestDTO.js';

const parseRequestShorthand = (requestString) => {
    const parsed = requestString.split('::');
    return {
        procedure: parsed[1],
        scope: parsed[0] || DEFAULT_REQUEST_SCOPE,
        version: parsed[2] || DEFAULT_REQUEST_VERSION,
    };
};

export { parseRequestShorthand };
//# sourceMappingURL=request.js.map
