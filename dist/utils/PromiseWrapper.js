import { __awaiter } from '../node_modules/tslib/tslib.es6.js';

const DEFAULT_TIMEOUT = 30000;
class PromiseWrapper {
    constructor(timeout) {
        this.reject = (rejectReturnValue) => {
            if (this.rejectPromise) {
                this.rejectPromise(rejectReturnValue);
            }
        };
        this.resolve = (resolveReturnValue) => {
            if (this.resolvePromise) {
                this.resolvePromise(resolveReturnValue);
            }
        };
        this.promise = new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            const timer = setTimeout(() => {
                reject(new Error('PromiseWraper timeout'));
            }, timeout || DEFAULT_TIMEOUT);
            this.rejectPromise = (arg) => {
                clearTimeout(timer);
                reject(arg);
            };
            this.resolvePromise = (arg) => {
                clearTimeout(timer);
                resolve(arg);
            };
        }));
    }
}

export { PromiseWrapper };
//# sourceMappingURL=PromiseWrapper.js.map
