let lsDebug = null;
try {
    lsDebug = localStorage.getItem('debug') || '';
}
catch (e) {
    if (typeof window !== 'undefined' && typeof localStorage !== 'undefined') {
        console.warn('Error checking window.debug');
    }
}
const GetDebugLogger = (prefix) => {
    if (lsDebug) {
        if (new RegExp(lsDebug).test(prefix)) {
            return (...args) => console.log(prefix, ...args);
        }
    }
    return (() => undefined);
};

export { GetDebugLogger };
//# sourceMappingURL=debug-logger.js.map
