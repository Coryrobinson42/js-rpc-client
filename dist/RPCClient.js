import { __awaiter } from './node_modules/tslib/tslib.es6.js';
import { CallRequestDTO } from './CallRequestDTO.js';
import { ClientManager } from './manager/ClientManager.js';
import { BrowserCache } from './cache/BrowserStorageCache.js';
import { InMemoryCache } from './cache/InMemoryCache.js';
import { HTTPTransport } from './transports/HTTPTransport.js';
import { WebSocketTransport } from './transports/WebSocketTransport.js';
import { cloneDeep, merge } from 'lodash-es';
import { parseRequestShorthand } from './utils/request.js';

const ERRORS = {
    HTTP_HOST_OR_TRANSPORT_REQUIRED: `http host or tansport is required`,
    INTERCEPTOR_MUSTBE_FUNC: `interceptors must be a function`,
};
class RPCClient {
    constructor(options) {
        var _a, _b, _c, _d, _e, _f;
        this.options = options;
        this.call = (request, args) => __awaiter(this, void 0, void 0, function* () {
            if (!request)
                throw new Error('RPCClient.call(request) requires a "request" param');
            let req;
            if (typeof request === 'string')
                req = parseRequestShorthand(request);
            else
                req = request;
            if (args) {
                req.args = args;
            }
            const requestDTO = new CallRequestDTO(req);
            if (!requestDTO.procedure && !requestDTO.identity) {
                throw new TypeError('RPCClient#call requires a "identity" or "procedure" prop and received neither');
            }
            if (!requestDTO.identity)
                requestDTO.identity = {};
            requestDTO.identity = merge(Object.assign({}, this.identity), requestDTO.identity);
            const callResponse = yield this.callManager.manageClientRequest(requestDTO);
            if (!callResponse.success) {
                throw callResponse;
            }
            return callResponse;
        });
        this.clearCache = (request) => {
            this.callManager.clearCache(request);
        };
        this.getCallCache = (request, args) => {
            let req;
            if (typeof request === 'string')
                req = parseRequestShorthand(request);
            else
                req = request;
            if (args) {
                req.args = args;
            }
            const cachedResponse = this.callManager.getCachedResponse(req);
            if (cachedResponse)
                return cachedResponse;
            return undefined;
        };
        this.getIdentity = () => this.identity ? cloneDeep(this.identity) : this.identity;
        this.getInFlightCallCount = () => {
            return this.callManager.getInFlightCallCount();
        };
        this.getWebSocketConnected = () => {
            var _a;
            return !!((_a = this === null || this === void 0 ? void 0 : this.webSocketTransport) === null || _a === void 0 ? void 0 : _a.isConnected());
        };
        this.makeProcedure = (request) => {
            const self = this;
            let req;
            if (typeof request === 'string')
                req = parseRequestShorthand(request);
            else
                req = request;
            return function curriedProcedure(args) {
                return self.call(Object.assign(Object.assign({}, req), { args }));
            };
        };
        this.registerResponseInterceptor = (responseInterceptor) => {
            this.callManager.addResponseInterceptor(responseInterceptor);
        };
        this.registerRequestInterceptor = (requestInterceptor) => {
            this.callManager.addRequestInterceptor(requestInterceptor);
        };
        this.setIdentity = (identity) => {
            this.identity = identity;
            this.callManager.setIdentity(identity);
        };
        if (!((_a = options === null || options === void 0 ? void 0 : options.hosts) === null || _a === void 0 ? void 0 : _a.http) && !((_b = options === null || options === void 0 ? void 0 : options.transports) === null || _b === void 0 ? void 0 : _b.http)) {
            throw new Error(ERRORS.HTTP_HOST_OR_TRANSPORT_REQUIRED);
        }
        if (options.requestInterceptor &&
            typeof options.requestInterceptor !== 'function') {
            throw new Error(ERRORS.INTERCEPTOR_MUSTBE_FUNC);
        }
        if (options.responseInterceptor &&
            typeof options.responseInterceptor !== 'function') {
            throw new Error(ERRORS.INTERCEPTOR_MUSTBE_FUNC);
        }
        let cache;
        const cacheOptions = { cacheMaxAgeMs: options === null || options === void 0 ? void 0 : options.cacheMaxAgeMs };
        if (options.cacheType == 'browser') {
            cache = new BrowserCache(cacheOptions);
        }
        else {
            cache = new InMemoryCache(cacheOptions);
        }
        const transports = [];
        if ((_c = options.transports) === null || _c === void 0 ? void 0 : _c.websocket) {
            transports.push(options.transports.websocket);
        }
        else if ((_d = options === null || options === void 0 ? void 0 : options.hosts) === null || _d === void 0 ? void 0 : _d.websocket) {
            this.webSocketTransport = new WebSocketTransport({
                host: options.hosts.websocket,
                onConnectionStatusChange: options.onWebSocketConnectionStatusChange,
            });
            transports.push(this.webSocketTransport);
        }
        if ((_e = options.transports) === null || _e === void 0 ? void 0 : _e.http) {
            transports.push(options.transports.http);
        }
        else if ((_f = options === null || options === void 0 ? void 0 : options.hosts) === null || _f === void 0 ? void 0 : _f.http) {
            this.httpTransport = new HTTPTransport({ host: options.hosts.http });
            transports.push(this.httpTransport);
        }
        this.callManager = new ClientManager({
            cache,
            deadlineMs: options.deadlineMs || RPCClient.DEFAULT_DEADLINE_MS,
            requestInterceptor: options.requestInterceptor,
            responseInterceptor: options.responseInterceptor,
            transports,
        });
    }
}
RPCClient.DEFAULT_DEADLINE_MS = 10000;

export { RPCClient };
//# sourceMappingURL=RPCClient.js.map
