const DEFAULT_REQUEST_SCOPE = 'global';
const DEFAULT_REQUEST_VERSION = '1';
class CallRequestDTO {
    constructor(call) {
        const { args, correlationId, identity, procedure, scope, version } = call;
        this.args = args;
        if (correlationId && typeof correlationId !== 'string')
            throw new Error('correlationId must be a string');
        this.correlationId = correlationId || Math.random().toString();
        if (identity) {
            if (typeof identity !== 'object')
                throw new Error('identity must be an object');
            if (identity.authorization && typeof identity.authorization !== 'string')
                throw new Error('identity.authorization must be a string');
            if (identity.deviceName && typeof identity.deviceName !== 'string')
                throw new Error('identity.deviceName must be a string');
            if (identity.metadata && typeof identity.metadata !== 'object')
                throw new Error('identity.metadata must be a object');
            this.identity = identity;
        }
        if (procedure && typeof procedure !== 'string')
            throw new Error('procedure must be string');
        this.procedure = procedure;
        if (scope && typeof scope !== 'string')
            throw new Error('scope must be string');
        this.scope = scope;
        if (version && typeof version !== 'string')
            throw new Error('version must be string');
        this.version = version;
    }
}

export { CallRequestDTO, DEFAULT_REQUEST_SCOPE, DEFAULT_REQUEST_VERSION };
//# sourceMappingURL=CallRequestDTO.js.map
