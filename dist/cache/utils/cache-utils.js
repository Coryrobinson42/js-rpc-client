import stringify from 'fast-json-stable-stringify';
import sortKeys from '../../utils/sort-keys.js';

const makeCallRequestKey = (request) => {
    const strippedCallRequest = {
        args: request.args,
        procedure: request.procedure,
        scope: request.scope,
        version: request.version,
    };
    let key;
    if (Array.isArray(strippedCallRequest) ||
        typeof strippedCallRequest === 'object') {
        key = stringify(sortKeys(strippedCallRequest, { deep: true }));
    }
    else {
        console.warn('makeCallRequestKey() was not passed a CallRequestDTO', request);
        throw new Error('makeCallRequestKey() was not passed a CallRequestDTO');
    }
    return key;
};

export { makeCallRequestKey };
//# sourceMappingURL=cache-utils.js.map
