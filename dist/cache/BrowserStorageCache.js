class BrowserCache {
    constructor(cacheOptions) {
        this.cacheOptions = cacheOptions;
    }
    clearCache() {
    }
    getCachedResponse(request) {
        return undefined;
    }
    setCachedResponse(request, response) {
    }
}
BrowserCache.DEFAULT_CACHE_MAX_AGE_MS = 1000 * 60 * 5;
BrowserCache.DEFAULT_CACHE_MAX_SIZE = 100;

export { BrowserCache };
//# sourceMappingURL=BrowserStorageCache.js.map
