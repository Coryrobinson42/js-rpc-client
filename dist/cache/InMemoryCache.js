import LRU from 'lru-cache';
import { makeCallRequestKey } from './utils/cache-utils.js';
import { GetDebugLogger } from '../utils/debug-logger.js';

const debug = GetDebugLogger('rpc:InMemoryCache');
class InMemoryCache {
    constructor(cacheOptions) {
        this.clearCache = () => {
            var _a;
            debug('clearCache');
            (_a = this.cachedResponseByParams) === null || _a === void 0 ? void 0 : _a.reset();
        };
        this.getCachedResponse = (request) => {
            var _a;
            debug('getCachedResponse, key: ', request);
            let cachedResponse = (_a = this.cachedResponseByParams) === null || _a === void 0 ? void 0 : _a.get(makeCallRequestKey(request));
            if (typeof cachedResponse === 'string') {
                cachedResponse = JSON.parse(cachedResponse);
            }
            return cachedResponse;
        };
        this.setCachedResponse = (request, response) => {
            var _a, _b;
            debug('setCachedResponse', request, response);
            const requestKey = makeCallRequestKey(request);
            if (!response) {
                return (_a = this.cachedResponseByParams) === null || _a === void 0 ? void 0 : _a.del(requestKey);
            }
            const cachedResponse = Object.assign({}, response);
            delete cachedResponse.correlationId;
            (_b = this.cachedResponseByParams) === null || _b === void 0 ? void 0 : _b.set(requestKey, cachedResponse ? JSON.stringify(cachedResponse) : undefined);
        };
        this.cachedResponseByParams = new LRU({
            max: (cacheOptions === null || cacheOptions === void 0 ? void 0 : cacheOptions.cacheMaxSize) || InMemoryCache.DEFAULT_CACHE_MAX_SIZE,
            maxAge: (cacheOptions === null || cacheOptions === void 0 ? void 0 : cacheOptions.cacheMaxAgeMs) || InMemoryCache.DEFAULT_CACHE_MAX_AGE_MS,
        });
    }
}
InMemoryCache.DEFAULT_CACHE_MAX_AGE_MS = 1000 * 60 * 5;
InMemoryCache.DEFAULT_CACHE_MAX_SIZE = 100;

export { InMemoryCache };
//# sourceMappingURL=InMemoryCache.js.map
