import autoExternal from 'rollup-plugin-auto-external'
import typescript from '@rollup/plugin-typescript'
import sourcemaps from 'rollup-plugin-sourcemaps'

import pkg from './package.json'

export default {
  input: 'src/index.ts',
  output: [
    {
      file: 'dist/cjs.js',
      format: 'cjs',
      sourcemap: true,
    },
    {
      dir: 'dist/',
      format: 'es',
      preserveModules: true,
      preserveModulesRoot: 'src/',
      sourcemap: true,
    },
  ],
  external: [...Object.keys(pkg.peerDependencies || {})],
  plugins: [
    autoExternal(),
    typescript(),
    // resolve sourcemaps to original code
    sourcemaps(),
  ],
}
